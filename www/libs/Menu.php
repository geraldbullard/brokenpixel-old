<?php

/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: Menu.php, v1.0 2015-08-19 maestro Exp $
 * @location /public/libs/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class Menu
{

    /* Private variables */
    var $_menu = array();

    /* Class constructor */
    public function __construct()
    {
        $this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);

        $this->_items = array();
        // Dashboard Menu Item (Always First)
        $q_dashboard = $this->db->select("SELECT * FROM `" . DB_PREFIX . "modules` WHERE `module` = 'admin' LIMIT 1");
        foreach ($q_dashboard as $d => $item) {
            $this->_items[] = array(
                'id' => $item['id'],
                'item' => '/' . $item['module'],
                'title' => $item['title'],
                'summary' => $item['summary'],
                'theme' => $item['theme'],
                'icon' => $item['icon'],
                'sort' => $item['sort'],
                'visibility' => $item['visibility'],
                'status' => $item['status'],
                'system' => $item['system'],
            );
        }

	    // all other menu items
	    $q_menu_items = $this->db->select("SELECT * FROM `" . DB_PREFIX . "modules` WHERE `status` = 1 AND `system` = 0 AND `theme` = 'admin' ORDER BY `sort`, `title` ASC");
	    foreach ($q_menu_items as $l => $item) {
            $q_menu[] = array(
                'id' => $item['id'],
                'item' => '/' . $item['module'],
                'title' => $item['title'],
                'summary' => $item['summary'],
                'theme' => $item['theme'],
                'icon' => $item['icon'],
                'sort' => $item['sort'],
                'visibility' => $item['visibility'],
                'status' => $item['status'],
                'system' => $item['system'],
            );
	    }
        array_multisort(array_column($q_menu, 'sort'), SORT_ASC, $q_menu);
        $this->_items = array_merge($this->_items, $q_menu);

        // Core Menu Items (Always Available)
        $q_core_items = $this->db->select("SELECT * FROM `" . DB_PREFIX . "modules` WHERE `module` != 'admin' AND `status` = 1 AND `system` = 1 AND `theme` = 'admin' ORDER BY `sort`, `title` ASC");
        foreach ($q_core_items as $c => $item) {
            $q_core[] = array(
                'id' => $item['id'],
                'item' => '/' . $item['module'],
                'title' => $item['title'],
                'summary' => $item['summary'],
                'theme' => $item['theme'],
                'icon' => $item['icon'],
                'sort' => $item['sort'],
                'visibility' => $item['visibility'],
                'status' => $item['status'],
                'system' => $item['system'],
            );
        }
        array_multisort(array_column($q_core, 'sort'), SORT_ASC, $q_core);
        $this->_items = array_merge($this->_items, $q_core);
    }

}
