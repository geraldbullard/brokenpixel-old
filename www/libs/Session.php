<?php

/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: Session.php, v1.0 2015-08-19 maestro Exp $
 * @location /public/libs/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class Session
{

    public static function init()
    {
        @session_start();
    }

    public static function set($key = '', $value = '')
    {
        $_SESSION[$key] = $value;
    }

    public static function setHeaderCssInclude($module = '', $file = '')
    {
        $exists = false;
        if (isset($_SESSION[$module]['headerCssInclude'])) {
            foreach ($_SESSION[$module]['headerCssInclude'] as $inc) {
                if ($inc == $file) $exists = true;
            }
        }
        if ($exists === false) $_SESSION[$module]['headerCssInclude'][] = $file;
    }

    public static function setFooterJsInclude($module = '', $file = '')
    {
        $exists = false;
        if (isset($_SESSION[$module]['footerJsInclude'])) {
            foreach ($_SESSION[$module]['footerJsInclude'] as $inc) {
                if ($inc == $file) $exists = true;
            }
        }
        if ($exists === false) $_SESSION[$module]['footerJsInclude'][] = $file;
    }

    public static function setNull($key = '')
    {
        $_SESSION[$key] = null;
    }

    public static function get($key = '')
    {
        $the_key = '';
        if (isset($_SESSION[$key])) {
            $the_key = $_SESSION[$key];
        }

        return $the_key;
    }

    public static function destroy()
    {
        session_destroy();
    }

    public static function logout()
    {
        Session::destroy();
        exit;
    }

    public static function loginCheck($username = '', $password = '')
    {
        $db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
        return $db->select(
            "SELECT " . DB_PREFIX . "users.*, " . DB_PREFIX . "roles.name as rolename
            FROM " . DB_PREFIX . "users
            LEFT JOIN " . DB_PREFIX . "roles
            ON " . DB_PREFIX . "users.id = " . DB_PREFIX . "roles.id
            WHERE " . DB_PREFIX . "users.username = '" . $username . "'
            AND " . DB_PREFIX . "users.password = '" . $password . "'
            LIMIT 1"
        );
    }

    public static function setSessionAccess($role_id = '')
    {
        foreach (Module::modules() as $module) {
	        $db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
        	$acc = $db->select(
                "SELECT access
                    FROM " . DB_PREFIX . "access
                    WHERE module = '" . $module['module'] . "'
                    AND roles_id = '" . (int)$role_id . "'"
            );
            Session::set($module['module'] . '_access', $acc[0]['access']);
        }
        return true;
    }

    public static function getSessionRoleName($role_id = '')
    {
	    $db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
    	$sql = $db->select(
            "SELECT name
                FROM " . DB_PREFIX . "roles
                WHERE id = :id", array(':id' => $role_id)
        );
        return $sql[0]['name'];
    }

    public static function setStatusMessage($key = '', $value = '', $status = '')
    {
        $_SESSION[$key] = array(
            'value' => $value,
            'status' => $status,
        );
    }

    public static function getStatusMessage($key = '')
    {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }
        return false;
    }

    // fix me later
    /*public static function setcookie(
        $name,
        $value = null,
        $expires = 0,
        $path = null,
        $domain = null,
        $secure = false,
        $httpOnly = false
    ) {
        global $request_type;

        if (empty($path)) {
            $path = ($request_type == 'https') ? HTTPS_URL : URL;
        }

        if (empty($domain)) {
            $domain = COOKIE_DOMAIN;
        }

        header(
            'Set-Cookie: ' . $name . '=' . urlencode($value) . ';
            expires=' . @date('D, d-M-Y H:i:s T', $expires) . ';
            path=' . $path . ';
            domain=' . $domain . (($secure === true) ? ' secure;' : '') . (($httpOnly === true) ? ' httponly;' : '')
        );
    }*/
    
}
