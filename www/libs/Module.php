<?php

/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: Module.php, v1.0 2015-08-19 maestro Exp $
 * @location /public/libs/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class Module
{
    /* Private variables */
    var $_modules = array();

    /* Class constructor */
    public function __construct()
    {
        $this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);

        $q_modules = $this->db->select(
            'SELECT * 
            FROM ' . DB_PREFIX . 'modules
            ORDER BY sort ASC'
        );

        foreach ($q_modules as $l => $module) {
            $this->_modules[$module['module']] = array(
                'id' => $module['id'],
                'module' => $module['module'],
                'title' => $module['title'],
                'summary' => $module['summary'],
                'theme' => $module['theme'],
                'icon' => $module['icon'],
                'sort' => $module['sort'],
                'visibility' => $module['visibility'],
                'status' => $module['status'],
                'system' => $module['system']
            );
        }

        $q_modules = null;
    }

    public static function modules()
    {
        global $Modules;
        return $Modules;
    }

    public static function getModuleCodeById($id = '')
    {
        global $Modules;
        foreach ($Modules as $m => $module) {
            if ($module['id'] == $id)
                return $module['module'];
        }
        return false;
    }

    public static function getModuleNameByModule($_module = '')
    {
        global $Modules;
        foreach ($Modules as $m => $module) {
            if ($module['module'] == $_module)
                return $module['title'];
        }
        return false;
    }

    public static function getModuleIcon($_module = '')
    {
        global $Modules;
        foreach ($Modules as $m => $module) {
            if ($module['module'] == $_module)
                return $module['icon'];
        }
        return false;
    }

    public function getAllActions()
    {
        global $Modules;
        foreach ($Modules as $m => $_module) {
            $acts = $this->db->select(
                "SELECT action
                    FROM " . DB_PREFIX . "modules_actions
                    WHERE `module` = '" . $m . "'"
            );
            foreach ($acts as $k => $v) {
                foreach ($v as $_action) {
                    $all_actions[][$m] = $m . '_' . $_action;
                }
            }
        }
        return $all_actions;
    }

    public function getModuleActions($module = '')
    {
        $acts = $this->db->select(
            "SELECT action
                FROM " . DB_PREFIX . "modules_actions
                WHERE `module` = '" . $module . "'"
        );
        foreach ($acts as $k => $v) {
            foreach ($v as $_action) {
                $actions[] = $module . '_' . $_action;
            }
        }
        return $actions;
    }

    public function getAdminModules()
    {
        global $Modules;
        return Utils::array_sort_by_column($Modules, 'sort');
    }

    public function moduleInDb($module = '')
    {
        $module = $this->db->select(
            "SELECT id FROM " . DB_PREFIX . "modules WHERE module = '" . $module . "' LIMIT 1"
        );
        // check modules table against module `code`
        if (@$module[0]['id'] != '')
            return $module[0]['id'];

        return false;
    }

    /*public function getDbModules()
    {
        // check modules table against module `code`
        if ($rs = pdologged_Query("SELECT * FROM modules;")) {
            while($row = $rs->fetch(PDO::FETCH_ASSOC))
            {
                $module = array(
                    'id' => $row['id'],
                    'title' => $row['title'],
                    'summary' => $row['summary'],
                    'code' => $row['code'],
                    'status' => $row['status'],
                    'core' => $row['core'],
                    'visibility' => $row['visibility']
                );
                $modules_list[] = $module;
            }
            return $modules_list;
        }
        return false;
    }*/

    /*public function getUninstalledModules()
    {
        // get existing module names
        $module_names = array();
        foreach (getDbModules() as $module) {
            $module_names[] = $module['code'];
        }

        // scan ALL module folders so we can compare
        $module_folders = array_diff(scandir($_SERVER['DOCUMENT_ROOT'] . '/modules'), array('..', '.'));

        // compare all found folders to existing installed modules and create array of uninstalled modules
        $uninstalled_modules = array();
        foreach ($module_folders as $module_folder) {
            if (!in_array($module_folder, $module_names)) {
                $uninstalled_modules[] = $module_folder;
            }
        }

        // check against "pictures" and create array of uninstalled mods
        $uninstalled = array();
        foreach($uninstalled_modules as $uninstalled_mod) {
            if ($uninstalled_mod != 'pictures') { // later we check for "hidden"
                $uninstalled[] = $uninstalled_mod;
            }
        }

        return $uninstalled;
    }*/

    /*public function hasDependencies($this_module = '')
    {
        global $Modules;
        foreach ($Modules as $module) {
            if (!empty($module->dependencies) && $module->dependencies[0] != 'Core')
                if ($module->name == $this_module)
                    return true;
        }
        return false;
    }*/

    /*public function checkDependencies($modules = '')
    {
        if (!is_array($modules))
            $modules = array($modules);

        foreach ($modules as $check_module) {
            foreach (getDbModules() as $db_module) {
                if ($check_module == $db_module['title'])
                    if ($db_module['status'] !== 1)
                        return false;
            }
        }
        return true;
    }*/

    public function getDependencies($this_module='')
    {
        global $Modules;
        foreach ($Modules as $module) {
            if (count(@$module->dependencies) > 0)
                if ($module->name == $this_module)
                    return $module->dependencies;
        }
        return false;
    }

    /*public function getOtherDependencies($this_module = '')
    {
        global $Modules;
        foreach ($Modules as $module) {
            if ($module->status > 0) {
                $dep[$module->name] = getDependencies($module->name);
                if (!empty($dep[$module->name])) {
                    foreach ($dep[$module->name] as $depend) {
                        $check[] = strtolower($depend);
                    }
                }
            }
            $dep = null;
        }
        if (!empty($check))
            if (in_array($this_module, $check))
                return true;
        return false;
    }*/

    /*public static function isCoreModule($module_code = '')
    {
        // check modules table against module `code`
        $moduleSQL = "SELECT core FROM modules WHERE code = '" . $module_code . "' LIMIT 1";
        $preprare = Tabmin::$db->prepare($moduleSQL);
        $preprare->execute();
        $module = $preprare->fetchAll();

        if (@$module[0]['core'] == 1)
            return true;

        return false;
    }*/

}
