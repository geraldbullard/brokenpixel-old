<?php

/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: Option.php, v1.0 2015-08-19 maestro Exp $
 * @location /public/libs/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class Option
{

	/* Private variables */
	var $_options = array();
	protected static $data;

	const CORE_OPTION = 1;
	const DESIGN_OPTION = 2;
	const SEO_OPTION = 3;
	const CUSTOM_OPTION = 4;

	/* Class constructor */
	public function __construct()
	{
		$this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
		$q_options = $this->db->select('select `define`, `value`, `type` from ' . DB_PREFIX . 'options');

		foreach ($q_options as $l => $option) {
			$this->_options[$option['type']][$option['define']] = $option['value'];
		}

		$q_options = null;
	}

	public static function loadAllTypes()
	{
		if (!isset(self::$data)) {
			self::lazyLoadTypes();
		}

		return self::$data;
	}

	public static function loadTypeById($id)
	{
		if (!isset(self::$data)) {
			self::lazyLoadTypes();
		}

		for ($i = 0; $i < count(self::$data); $i++) {
			if (self::$data[$i]->getId() == $id) {
				return self::$data[$i];
			}
		}

		return false;
	}

	protected static function lazyLoadTypes()
	{
		self::$data = array
		(
			new OptionType(array
			(
				'id' => self::CORE_OPTION,
				'title' => 'Core',
				'code' => 'core',
			)),
			new OptionType(array
			(
				'id' => self::DESIGN_OPTION,
				'title' => 'Design',
				'code' => 'design',
			)),
			new OptionType(array
			(
				'id' => self::SEO_OPTION,
				'title' => 'SEO',
				'code' => 'seo',
			)),
			new OptionType(array
			(
				'id' => self::CUSTOM_OPTION,
				'title' => 'Custom',
				'code' => 'custom',
			))
		);
	}

	public static function getOptionById($id = '')
	{
		$db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
		$o_val = $db->select(
			"SELECT * FROM " . DB_PREFIX . "options WHERE id = '" . $id . "' LIMIT 1"
		);

		return $o_val[0];
	}

	public static function getOptionValueById($id = '')
	{
		$db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
		$o_val = $db->select(
			"SELECT value FROM " . DB_PREFIX . "options WHERE id = '" . $id . "' LIMIT 1"
		);

		return $o_val[0]['value'];
	}

	/* Class constructor */
	/*public function getOptionTypes()
    {
        $this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
        return $this->db->select('select distinct `type` from ' . DB_PREFIX . 'options');
    }*/

	public static function getOptionsCount()
	{
		$conn = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
		$count = $conn->select(
			"SELECT COUNT(*) AS `count` 
            FROM " . DB_PREFIX . "options;"
		);

		return $count[0]['count'];
	}

}

class OptionType
{
	protected $id;
	protected $title;
	protected $code;

	public function __construct($properties)
	{
		foreach ($properties as $property => $value) {
			if (property_exists($this, $property)) {
				$this->{"$property"} = $value;
			}
		}
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @return string
	 */
	public function getCode()
	{
		return $this->code;
	}

}
