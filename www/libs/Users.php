<?php

/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: Users.php, v1.0 2015-08-19 maestro Exp $
 * @location /public/libs/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class Users
{

    /* Private variables */
    var $_users = array();

    /* Class constructor */
    public static function loadAll()
    {
        $conn = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
        $q_users = $conn->select("SELECT * FROM " . DB_PREFIX . "users");

        foreach ($q_users as $key => $value) {
            $users[$key] = $value;
        }

        return $users;
    }

    public static function loadById($id)
    {
        $conn = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
        $user = $conn->select(
            "SELECT * 
            FROM " . DB_PREFIX . "users 
            WHERE `id` = :id LIMIT 1",
            array(':id' => $id)
        );

        return $user[0];
    }

    public static function getFullName($id)
    {
        $data = self::loadById($id);

        return $data['firstname'] . ' ' . $data['lastname'];
    }

    public static function getUsersCount()
    {
        $conn = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
        $count = $conn->select(
            "SELECT COUNT(*) AS `count` 
            FROM " . DB_PREFIX . "users;"
        );

        return $count[0]['count'];
    }

    public static function getRolesCount()
    {
        $conn = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
        $count = $conn->select(
            "SELECT COUNT(*) AS `count` 
            FROM " . DB_PREFIX . "roles;"
        );

        return $count[0]['count'];
    }

}

class SingleUser
{

    protected $id;
    protected $firstname;
    protected $lastname;
    protected $email;
    protected $username;
    protected $gender;
    protected $roles_id;
    protected $status;
    protected $created;

    public function __construct($properties)
    {
        foreach ($properties as $property => $value) {
            if (property_exists($this, $property)) {
                $this->{"$property"} = $value;
            }
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFirstName()
    {
        return $this->firstname;
    }

    public function getLastName()
    {
        return $this->lastname;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getUserName()
    {
        return $this->username;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getCreated()
    {
        return $this->created;
    }

}
