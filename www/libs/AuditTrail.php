<?php

/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: AuditTrail.php, v1.0 2015-08-19 maestro Exp $
 * @location /public/libs/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class AuditTrail
{
    protected static $data;

    const USER_LOGIN = 1;
    const USER_LOGOUT = 2;
    const USER_ACTION = 3;
    const RECORD_CREATED = 4;
    const RECORD_EDITED = 5;
    const RECORD_DELETED = 6;
    const MODULE_ENABLED = 7;
    const MODULE_DISABLED = 8;
    const MODULE_DELETED = 9;

    public static function loadAll($where = '', $order_by = '', $limit = '') {
        $conn = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);

        if ($where != '')
            $where = ' WHERE ' . $where;

        if ($order_by != '')
            $order_by = ' ORDER BY ' . $order_by;

        if ($limit != '')
            $limit = ' LIMIT ' . $limit;

        $result = $conn->select(
            'SELECT * 
            FROM ' . DB_PREFIX . 'audit' .
            $where . $order_by . $limit
        );

        return $result;
    }

    public static function loadAllTypes()
    {
        if (!isset(self::$data)) {
            self::lazyLoadTypes();
        }

        return self::$data;
    }

    public static function loadTypeById($id)
    {
        if (!isset(self::$data)) {
            self::lazyLoadTypes();
        }

        for ($i = 0; $i < count(self::$data); $i++) {
            if (self::$data[$i]->getId() == $id) {
                return self::$data[$i];
            }
        }

        return false;
    }

    public static function log($at = '', $ud = '', $url = '', $msg = '', $rid = '') {
    	$conn = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
        $module = explode("/", $url);
        $result = $conn->insert(DB_PREFIX . 'audit', array(
            'audit_types_id' => $at,
            'users_id' => $ud['usersId'],
            'url' => $url,
            'remote_host' => $ud['usersIpAddress'],
            'timestamp' => date("Y-m-d H:i:s"), // adjusted for timezone offset from UTC
            'user_agent' => $ud['usersAgent'],
            'message' => $msg,
            'module' => $module[0],
            'related_id' => $rid
        ));

        if ($result)
            return true;
        return false;
    }

    protected static function lazyLoadTypes()
    {
        self::$data = array
        (
            new AuditType(array
            (
                'id' => self::USER_LOGIN,
                'title' => 'User Login',
                'activity_label' => 'Login',
            )),
            new AuditType(array
            (
                'id' => self::USER_LOGOUT,
                'title' => 'User Logout',
                'activity_label' => 'Logout',
            )),
            new AuditType(array
            (
                'id' => self::USER_ACTION,
                'title' => 'User Action',
                'activity_label' => 'Action',
            )),
            new AuditType(array
            (
                'id' => self::RECORD_CREATED,
                'title' => 'Record Created',
                'activity_label' => 'Created',
            )),
            new AuditType(array
            (
                'id' => self::RECORD_EDITED,
                'title' => 'Record Edited',
                'activity_label' => 'Edited',
            )),
            new AuditType(array
            (
                'id' => self::RECORD_DELETED,
                'title' => 'Record Deleted',
                'activity_label' => 'Deleted',
            )),
            new AuditType(array
            (
                'id' => self::MODULE_ENABLED,
                'title' => 'Module Enabled',
                'activity_label' => 'Enabled',
            )),
            new AuditType(array
            (
                'id' => self::MODULE_DISABLED,
                'title' => 'Module Disabled',
                'activity_label' => 'Disabled',
            )),
            new AuditType(array
            (
                'id' => self::MODULE_DELETED,
                'title' => 'Module Deleted',
                'activity_label' => 'Deleted',
            ))
        );
    }

}

class AuditType
{
    protected $id;
    protected $title;
    protected $activity_label;

    public function __construct($properties)
    {
        foreach ($properties as $property => $value) {
            if (property_exists($this, $property)) {
                $this->{"$property"} = $value;
            }
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getActivityLabel()
    {
        return $this->activity_label;
    }

}
