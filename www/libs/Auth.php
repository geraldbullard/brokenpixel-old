<?php

/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: Auth.php, v1.0 2015-08-19 maestro Exp $
 * @location /libs/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class Auth
{

    public static function handleLogin($private = '')
    {
        @session_start();
        $logged = isset($_SESSION['loggedIn']);
        if ($private) {
            if ($logged == false) {
                session_destroy();
                header('location: /login');
                exit;
            }
        }
    }

}