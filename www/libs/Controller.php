<?php

/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: Controller.php, v1.0 2015-08-19 maestro Exp $
 * @location /libs/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class Controller
{
    public function __construct()
    {
        $this->view = new View();
    }

    /**
     * @param string $name Name of the model
     *
     * @param string $path Location of the models
     */
    public function loadModel($name, $modelPath = 'app/modules/')
    {
        $file = $modelPath . $name . '/model.php';

        if (file_exists($file)) {
            require_once($file);

            $modelName = $name . '_Model';
            $this->model = new $modelName();
        }
    }
}
