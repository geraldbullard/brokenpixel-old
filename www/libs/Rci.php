<?php

class Rci
{
    var $_folders = array();

    function __construct()
    {
        /* not sure if this will be needed in bPixel
        if (!isset($_SESSION['Rci_data'])) {
            $_SESSION['Rci_data'] = array('folders' => array());
        }
        $this->_folders =& $_SESSION['Rci_data']['folders'];*/
    }

    function get($viewName, $function, $display = true)
    {

        $viewName = strtolower($viewName);
        $function = strtolower($function);
        $rci_holder = '';

        // see if the page name is known
        if (!array_key_exists($viewName, $this->_folders)) {
            $this->_build_folder($viewName);
        }

        if (array_key_exists($function, $this->_folders[$viewName])) {
            foreach ($this->_folders[$viewName][$function] as $fileName) {
                // safety check in case a fasle positive was received on the cache check
                if (!file_exists(APP . 'runtime/' . $viewName . '/' . $fileName)) {
                    $this->_folders = array();  // invalid the cache since it is corrupt
                    continue;
                }
                $rci = '';
                include(APP . 'runtime/' . $viewName . '/' . $fileName);
                $rci_holder .= $rci;
            }
        }
        return $rci_holder;
    }

    function _build_folder($viewName) // module/view name
    {
        $this->_folders[$viewName] = array();
        if (is_dir(APP . 'runtime/' . $viewName)) {
            $filesFound = array();
            $pattern = '/(\w*)_*(\w+)_(\w+)_(\w+)\.php$/';
            $dir = opendir(APP . 'runtime/' . $viewName);
            while ($file = readdir($dir)) {
                if ($file == '.' || $file == '..') continue;
                $match = array();
                if (preg_match($pattern, $file, $match) > 0) {
                    if ($match[3] == $viewName) {
                        $filesFound[$match[0]] = $match[4];
                    }
                }
            }
            if (count($filesFound) > 0) {
                ksort($filesFound);
                foreach ($filesFound as $file => $function) {
                    $this->_folders[$viewName][$function][] = $file;
                }
            }
        }
    }

}
