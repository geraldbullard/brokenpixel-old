<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: ajax.php, v1.0 2019-05-19 maestro Exp $
 * @location /
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

require_once('app/db.php');
require_once('libs/Database.php');

$success = false;

if (isset($_POST['ajaxSort']) && $_POST['ajaxSort'] == true) {
    $conn = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);

    $postData = array('sort' => $_POST['sort']);
    $result = $conn->update(DB_PREFIX . 'content', $postData, "`id` = {$_POST['id']}");
    // add the success message to the session, and insert the audit trail db entry
    if ($result) {
        $success = true;
    }
}

echo json_encode(array('success' => $success));
exit;
