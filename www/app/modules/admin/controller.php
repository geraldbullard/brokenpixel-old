<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: controller.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/admin/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class Admin extends Controller {

    function __construct() {
        parent::__construct();
        // check if logged in
        if (Session::get('loggedIn') != true) {
            header('Location: /login/');
            exit;
        }
    }

    public function index() {
        // check for view permissions
        if (Session::get('admin_access') < ACCESS_VIEW) {
            // redirect if not enough permission
            header('Location: /noaccess');
            exit;
        } else {
            // continue to admin dashboard page if enough permission
            $this->view->getTheme('admin');
            $this->view->heading = 'Dashboard';
            $this->view->renderHeader();
            $this->view->renderView('admin/view/index');
            $this->view->renderFooter();
            $this->view->renderScripts();
            $this->view->renderModuleJs('admin/view/js/index.js.php');
            $this->view->renderBottom();
        }
    }

}
