<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: menu.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/admin/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

$_menu = array(
    'item' => '/admin',
    'title' => 'Dashboard',
    'icon' => 'fa-dashboard',
    'system' => 1,
);
