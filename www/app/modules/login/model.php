<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: model.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/login/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class login_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    public function run() {
        global $Modules;
        session_destroy();
        $sql = Session::loginCheck($_POST['username'], Hash::create('sha256', $_POST['password'], HASH_PASSWORD_KEY));

        if (!empty($sql)) {
            // login
            Session::init();
            Session::set('usersId', $sql[0]['id']);
            Session::set('usersFirstName', $sql[0]['firstname']);
            Session::set('usersLastName', $sql[0]['lastname']);
            Session::set('usersEmail', $sql[0]['email']);
            Session::set('usersUserName', $sql[0]['username']);
            Session::set('usersGender', $sql[0]['gender']);
            Session::set('usersRoleId', $sql[0]['roles_id']);
            Session::set('usersRoleName', $sql[0]['rolename']);
            Session::set('usersStatus', $sql[0]['status']);
            Session::set('usersCreatedDate', $sql[0]['created']);
            Session::set('loggedIn', true);
            Session::setSessionAccess($sql[0]['roles_id']);
            Session::set('usersData', array(
                'usersId' => $sql[0]['id'],
                'usersIpAddress' => Utils::get_client_ip(),
                'usersAgent' => $_SERVER['HTTP_USER_AGENT'],
            ));
	        // insert the autdit trail
	        AuditTrail::log(
		        AuditTrail::USER_LOGIN,
		        Session::get('usersData'),
		        'login/',
		        'User Login',
		        Session::get('usersId')
	        );
            if (Session::get('admin_access') > 0) {
                header('location: /admin');
                exit;
            } else {
                header('location: /index');
                exit;
            }
        } else {
            header('location: /login');
            exit;
        }
    }
    
}
