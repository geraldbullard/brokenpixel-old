<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: controller.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/login/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class Login extends Controller {

    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        // view the login page
        $this->view->getTheme('site');
        $this->view->renderHeader();
        $this->view->renderView('login/view/index');
        $this->view->renderFooter();
        $this->view->renderScripts();
        $this->view->renderBottom();
    }
    
    public function run()  {
        // run the login process
        $this->model->run();
    }

}
