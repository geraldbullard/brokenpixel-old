<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: index.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/login/view/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<h1><?php echo $Language->get('text_login'); ?></h1>
<div class="row">
    <div class="col-sm-6 col-lg-6 large-padding-left margin-top">
        <div class="well no-padding-top">
            <h3><?php echo $Language->get('text_existing') . ' ' . $Language->get('text_user'); ?></h3>
            <form role="form" id="login" name="login" class="no-margin-bottom" action="/login/run" method="post">
                <div class="form-group">
                    <label for="username" class="sr-only"><?php echo $Language->get('text_username'); ?></label>
                    <input type="text" name="username" class="form-control" placeholder="<?php echo $Language->get('text_username'); ?>"/>
                </div>
                <div class="form-group">
                    <label for="password" class="sr-only"><?php echo $Language->get('text_password'); ?></label>
                    <input type="password" name="password" class="form-control" placeholder="<?php echo $Language->get('text_password'); ?>"/>
                    <p class="help-block small-margin-left">
                        <a href="/forgotten_password">
                            <?php echo $Language->get('text_password') . ' ' . $Language->get('text_forgotten'); ?>
                        </a>
                    </p>
                </div>
                <div class="button-set clearfix">
                    <button class="pull-right btn btn-lg btn-primary" onclick="$('#login').submit();" type="submit">
                        <?php echo $Language->get('text_sign_in'); ?>
                    </button>
                </div>
            </form>
        </div>
    </div>
    <?php if ($Options['core']['allowRegistrations'] == '1') { ?>
    <div class="col-sm-6 col-lg-6 margin-top create-account-div">
        <div class="well no-padding-top">
            <h3><?php echo $Language->get('text_new') . ' ' . $Language->get('text_user'); ?></h3>
            <p><?php echo $Language->get('text_new_user_instruction'); ?></p>
            <div class="buttons-set clearfix large-margin-top">
                <form class="form-inline" action="/user/createuser" method="post">
                    <button class="pull-right btn btn-lg btn-primary" type="button" onclick="$(this).closest('form').submit();">
                        <?php echo $Language->get('text_create') . ' ' . $Language->get('text_account'); ?>
                    </button>
                </form>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
