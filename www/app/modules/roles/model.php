<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: model.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/roles/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class Roles_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    public function rolesList($type = '', $limit = '') {
        $where = '';
        if (isset($_GET['roles_filter']) && $_GET['roles_filter'] > 0) {
            $where = ' WHERE ';
            $arr = array();
            if ($_GET['roles_search'] != '') {
                $termsLike = array();
                foreach (explode(" ", $_GET['roles_search']) as $term) {
                    $termsLike[] = " `name` LIKE '%" . $term . "%' ";
                }
                $arr[] = '(' . implode(' OR ', $termsLike) . ')';
            }
            $where = $where . implode(" ", $arr);
            if ($_GET['roles_hidden'] != '')
                $where .= ' AND `hidden` = ' . $_GET['roles_hidden'];
        }
        if ($limit != '') $limit = ' LIMIT ' . $limit;
        /*echo "SELECT DISTINCT *
            FROM " . DB_PREFIX . "roles" .
            $where .
            $limit;
        die();*/
        return $this->db->select("
            SELECT DISTINCT *
            FROM " . DB_PREFIX . "roles" .
            $where .
            $limit
        );
    }

    public function rolesListFull() {
        return $this->db->select('SELECT * FROM ' . DB_PREFIX . 'roles');
    }

    public function modulesList() {
        return $this->db->select('SELECT * FROM ' . DB_PREFIX . 'modules WHERE module != "error"');
    }
    
    public function rolesSingleList($id = '') {
        return $this->db->select(
            'SELECT ' . DB_PREFIX . 'roles.id as roles_id, 
            ' . DB_PREFIX . 'roles.*, 
            ' . DB_PREFIX . 'access.module, 
            ' . DB_PREFIX . 'access.access 
            FROM ' . DB_PREFIX . 'roles 
            LEFT JOIN ' . DB_PREFIX . 'access 
            ON ' . DB_PREFIX . 'roles.id = ' . DB_PREFIX . 'access.roles_id 
            WHERE ' . DB_PREFIX . 'roles.id = :id',
            array(':id' => $id)
        );
    }
    
    public function create($data) {
        $this->db->insert(DB_PREFIX . 'roles', array(
            'name' => $data['name'],
            'hidden' => $data['hidden']
        ));
        $roles_id = $this->db->lastInsertId();
        // insert the autdit trail
        AuditTrail::log(
            AuditTrail::RECORD_CREATED,
            Session::get('usersData'),
            'roles/create/',
            'Role Created', // AuditTrail::loadTypeById(AuditTrail::RECORD_CREATED)->getTitle()
            $roles_id
        );
        foreach ($data['access'] as $a => $access) {
            $this->db->insert(DB_PREFIX . 'access', array(
                'roles_id' => (int)$roles_id,
                'module' => $a,
                'access' => $access
            ));
        }
    }
    
    public function editSave($data) {
        $postData = array(
            'id' => $data['id'],
            'name' => $data['name'],
            'hidden' => $data['hidden']
        );
        $roles = $this->db->update(DB_PREFIX . 'roles', $postData, "`id` = {$data['id']}");
        $remove = $this->db->delete(DB_PREFIX . 'access', "roles_id = '" . $data['id'] . "'", count($this->db->select("SELECT * FROM " . DB_PREFIX . "access WHERE `roles_id` = {$data['id']}")));
        $add = true;
        foreach ($data['access'] as $a => $access) {
            if (!$this->db->insert(DB_PREFIX . 'access', array(
                'roles_id' => (int)$data['id'],
                'module' => $a,
                'access' => $access
            ))) $add = false;
        }
        if ($roles && $remove && $add) {
            // add the success message to the session, and insert the audit trail db entry
            Session::setStatusMessage('rolesMessage', 'text_roles_save_success', 'success');
            // insert the autdit trail
            AuditTrail::log(
                AuditTrail::RECORD_EDITED,
                Session::get('usersData'),
                'roles/edit/',
                'Role Edited', // AuditTrail::loadTypeById(AuditTrail::RECORD_EDITED)->getTitle()
                $data['id']
            );
            return true;
        } else { // otherwise there was an error
            Session::setStatusMessage('rolesMessage', 'text_roles_save_error', 'danger');
            return false;
        }

    }

    public function delete($id) {
        $this->db->delete(DB_PREFIX . 'roles', "id = '$id'");
        $this->db->delete(DB_PREFIX . 'access', "roles_id = '$id'", count($this->db->select("SELECT * FROM " . DB_PREFIX . "access WHERE `roles_id` = {$id}")));
        // insert the autdit trail
        AuditTrail::log(
            AuditTrail::RECORD_DELETED,
            Session::get('usersData'),
            'roles/delete/',
            'Role Deleted', // AuditTrail::loadTypeById(AuditTrail::RECORD_DELETED)->getTitle()
            $id
        );
    }

    public function getRolesName($id = '') {
        $result = $this->db->select('SELECT name FROM ' . DB_PREFIX . 'roles WHERE id = :id');

        return $result;
    }

}
