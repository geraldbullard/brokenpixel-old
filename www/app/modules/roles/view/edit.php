<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: edit.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/roles/view/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<form method="post" action="/roles/editsave/<?php echo $this->role[0]['roles_id']; ?>">
    <div class="row">
        <div class="col-lg-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo $Language->get('text_role') . ' ' . $Language->get('text_information'); ?></h3>
                </div>
                <div class="panel-body">
                    <p>
                        <label for="name"><?php echo $Language->get('text_name'); ?></label>
                        <input
                            type="text"
                            name="name"
                            class="form-control"
                            value="<?php echo $this->role[0]['name']; ?>" />
                    </p>
                    <p>
                        <label for="hidden"><?php echo $Language->get('text_hidden'); ?></label>
                        <select name="hidden" class="form-control">
                            <option value="1" <?php echo ($this->role[0]['hidden']=='1') ? 'selected' : ''; ?>>
                                <?php echo $Language->get('text_yes'); ?>
                            </option>
                            <option value="0" <?php echo ($this->role[0]['hidden']=='0') ? 'selected' : ''; ?>>
                                <?php echo $Language->get('text_no'); ?>
                            </option>
                        </select>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <?php echo $Language->get('text_modules') . ' ' . $Language->get('text_access'); ?>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="role-access">
                        <?php
                        foreach (Module::modules() as $module) {
                            foreach ($this->role as $r => $role) {
                                if ($role['module'] == $module['module']) {
                                    $access = $role['access'];
                                }
                            }
                        ?>
                        <div class="access-row">
                            <label for="access_<?php echo $module['module']; ?>"><?php echo $module['title']; ?></label>
                            <input
                                type="text"
                                name="access[<?php echo $module['module']; ?>]"
                                id="access_<?php echo $module['module']; ?>"
                                value="<?php echo ($access ? $access : 0); ?>"
                                style="display:none;">
                            <div id="access_slider_<?php echo $module['module']; ?>">
                                <div id="custom_handle_<?php echo $module['module']; ?>" class="ui-slider-handle"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <p class="padding-top-10">
                <button type="submit" class="pull-left btn btn-lg btn-danger">
                    <?php echo $Language->get('text_save'); ?>
                </button>
                <button
                    type="button"
                    class="cancel pull-right btn btn-lg btn-default">
                    <?php echo $Language->get('text_cancel'); ?>
                </button>
            </p>
        </div>
    </div>
</form>
