<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: user.js.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/roles/view/js/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<script>
function getTextValue(val) {
    if (val == 0)
    return 'None';
    if (val == 1)
    return 'View';
    if (val == 2)
    return 'Edit';
    if (val == 3)
    return 'Add';
    if (val == 4)
    return 'Delete';
    if (val == 5)
    return 'Global';
}
$(document).ready(function () {
    $(function() {
        <?php
        foreach (Module::modules() as $module) {
        ?>
        $("#access_slider_<?php echo $module['module']; ?>").slider({
            value: $("#access_<?php echo $module['module']; ?>").val(),
            min: 0,
            max: 5,
            step: 1,
            create: function(){
                $("#custom_handle_<?php echo $module['module']; ?>").text(
                    getTextValue(
                        $("#access_slider_<?php echo $module['module']; ?>").slider("value")
                        )
                    );
                },
            slide: function(event, ui){
                $("#custom_handle_<?php echo $module['module']; ?>").text(getTextValue(ui.value));
                $("#access_<?php echo $module['module']; ?>").val(ui.value);
                }
            });
        <?php
        }
        ?>
    });
    $(".delete-role").confirm({
        title: "<?php echo $Language->get('text_delete') . ' ' . $Language->get('text_role'); ?>",
        content: "<?php echo $Language->get('text_roles_delete_confirm'); ?>",
        buttons: {
            ok: function() {
                location.href = '/roles/delete/' + this.$target.attr('data-role-id');
            },
            cancel: function() {
            }
        }
    });
    $(".cancel").confirm({
        title: "<?php echo $Language->get('text_cancel'); ?>",
        content: "<?php echo $Language->get('text_cancel_confirm'); ?>",
        buttons: {
            ok: function() {
                location.href = '/roles/';
            },
            cancel: function() {
            }
        }
    });
    /*$("[data-slider]").each(function() {
    }).bind("slider:ready slider:changed", function(event, data) {
        $(this).next(".slider-value").html(data.value.toFixed());
    });*/
});
</script>
