<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: controller.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/roles/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class Roles extends Controller {

    public function __construct() {
        parent::__construct();
        // check if logged in
        if (Session::get('loggedIn') != true) {
            header('Location: /login/');
            exit;
        }
    }
    
    public function index() {
        global $Language;
        // check for view access
        if (Session::get('roles_access') < ACCESS_VIEW) {
            // redirect if not enough access
            header('Location: /noaccess/');
            exit;
        } else {
            global $Options;
            // START Pagination
            $this->view->rpp = $Options['core']['rolesAdminPagination'];
            if (@$_GET['order']) $order = strtoupper($_GET['order']);
            if (@$_GET['page']) $page = intval(@$_GET['page']);
            if (@$page <= 0) $page = 1;
            $this->view->page = $page;
            $this->view->limit = (($page-1) * $this->view->rpp) . ',' . $this->view->rpp;
            if (isset($_GET['roles_filter']) && $_GET['roles_filter'] != '') {
                $this->view->params = array(
                    'roles_filter' => 1
                );
            }
            if (isset($_GET['roles_filter']) && $_GET['roles_filter'] != '') {
                $this->view->rolesList = $this->model->rolesList(@$order, $this->view->limit);
                $this->view->rolesListResults = count($this->model->rolesList(@$order));
            } else {
                $this->view->rolesList = $this->model->rolesList('', $this->view->limit);
                $this->view->rolesListResults = count($this->model->rolesList());
            }
            $this->view->totalPages = ceil($this->view->rolesListResults/$this->view->rpp);
            $this->view->getTheme('admin');
            $this->view->heading = $Language->get('text_roles');
            $this->view->renderHeader();
            $this->view->renderView('roles/view/index');
            $this->view->renderFooter();
            $this->view->renderScripts();
            $this->view->renderModuleJs('roles/view/js/index.js.php');
            $this->view->renderBottom();
        }
    }
    
    public function createRole() {
        global $Language;
        // check for create access
        if (Session::get('roles_access') < ACCESS_CREATE) {
            // redirect if not enough access
            header('Location: /noaccess/');
            exit;
        } else {
            // continue to create role page if enough access
            $this->view->modules = $this->model->modulesList();
            $this->view->getTheme('admin');
            $this->view->heading = $Language->get('text_create') . ' ' . $Language->get('text_role');
            $this->view->renderHeader();
            $this->view->renderView('roles/view/create');
            $this->view->renderFooter();
            $this->view->renderScripts();
            $this->view->renderModuleJs('roles/view/js/create-edit.js.php');
            $this->view->renderBottom();
        }
    }
    
    public function create() {
        // check for create access
        if (Session::get('roles_access') < ACCESS_CREATE) {
            // redirect if not enough access
            header('Location: /noaccess/');
            exit;
        } else {
            // create role if enough access
            $data = array();
            $data['name'] = $_POST['name'];
            $data['hidden'] = $_POST['hidden'];
            $data['access'] = $_POST['access'];

            // @TODO: Do your error checking!

            $this->model->create($data);
            header('location: /roles/');
            exit;
        }
    }
    
    public function edit($id = '') {
        global $Language;
        // check for edit access
        if (Session::get('roles_access') < ACCESS_EDIT) {
            // redirect if not enough access
            header('Location: /noaccess/');
            exit;
        } else {
            // check if id is empty
            if (empty($id)) {
                // redirect if id empty
                header('Location: /roles/');
                exit;
            }
            // continue to edit role page if enough access
            $this->view->modules = $this->model->modulesList();
            $this->view->role = $this->model->rolesSingleList($id);
            $this->view->getTheme('admin');
            $this->view->heading = $Language->get('text_edit') . ' ' .
                $Language->get('text_role') . ': ' . $this->view->role[0]['name'];
            $this->view->renderHeader();
            $this->view->renderView('roles/view/edit');
            $this->view->renderFooter();
            $this->view->renderScripts();
            $this->view->renderModuleJs('roles/view/js/create-edit.js.php');
            $this->view->renderBottom();
        }
    }
    
    public function editSave($id = '') {
        // check for edit access
        if (Session::get('roles_access') < ACCESS_EDIT) {
            // redirect if not enough access
            header('Location: /noaccess/');
            exit;
        } else {
            // save role if enough access
            $data = array();
            $data['id'] = $id;
            $data['name'] = $_POST['name'];
            $data['hidden'] = $_POST['hidden'];
            $data['access'] = $_POST['access'];

            // @TODO: Do your error checking!

            $this->model->editSave($data);
            header(
                'location: /roles/' . (
                    isset($_POST['page']) && $_POST['page'] != '' ? '?page=' . $_POST['page'] : ''
                )
            );
            exit;
        }
    }
    
    public function delete($id = '') {
        // check for delete access
        if (Session::get('roles_access') < ACCESS_DELETE) {
            // redirect if not enough access
            header('Location: /noaccess/');
            exit;
        } else {
            // delete role if enough access
            $this->model->delete($id);
            header('location: /roles/');
            exit;
        }
    }

}
