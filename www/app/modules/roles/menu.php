<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: menu.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/roles/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

$_menu = array(
    'item' => '/roles',
    'title' => 'Roles',
    'icon' => 'fa-sliders',
    'system' => 1,
    'sub_items' => array(
        'sub_item' => array(
            'item' => '/roles/createrole/',
            'title' => 'Create Role',
            'icon' => 'fa-plus',
            'system' => 1,
        ),
    ),
);
