<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: controller.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/audit/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class Audit extends Controller {

    function __construct() {
        parent::__construct();
        // check if logged in
        if (Session::get('loggedIn') != true) {
            header('Location: /login/');
            exit;
        }
    }

    public function index() {
        // check for view access
        if (Session::get('audit_access') < ACCESS_VIEW) {
            // redirect if not enough permission
            header('Location: /noaccess');
            exit;
        } else {
            global $Options;
            // START Pagination
            $this->view->rpp = $Options['core']['auditAdminPagination'];
            if (@$_GET['order']) $order = strtoupper($_GET['order']);
            if (@$_GET['page']) $page = intval(@$_GET['page']);
            if (@$page <= 0) $page = 1;
            $this->view->page = $page;
            $this->view->limit = (($page-1) * $this->view->rpp) . ',' . $this->view->rpp;
            // START Search Filter
            $this->view->modulesArr = array(
                array('module' => 'content', 'title' => 'Content'),
                array('module' => 'login', 'title' => 'Login'),
                array('module' => 'logout', 'title' => 'Logout'),
                array('module' => 'modules', 'title' => 'Modules'),
                array('module' => 'options', 'title' => 'Options'),
                array('module' => 'roles', 'title' => 'Roles'),
                array('module' => 'user', 'title' => 'Users'),
            );
            // added for pagination
            if (isset($_GET['audit_filter']) && $_GET['audit_filter'] != '') {
                $this->view->params = array(
                    'audit_filter' => 1,
                    'audit_user' => $_GET['audit_user'],
                    'audit_date_range' => $_GET['audit_date_range'],
                    'audit_module' => $_GET['audit_module'],
                );
            }
            // continue to epass page if enough access
            if (isset($_GET['audit_filter']) && $_GET['audit_filter'] != '') {
                $this->view->auditList = $this->model->auditList(@$order, $this->view->limit);
                $this->view->auditListResults = count($this->model->auditList(@$order));
            } else {
                $this->view->auditList = $this->model->auditList('', $this->view->limit);
                $this->view->auditListResults = count($this->model->auditList());
            }
            $this->view->totalPages = ceil($this->view->auditListResults/$this->view->rpp);
            $this->view->getTheme('admin');
            $this->view->heading = 'Audit History';
            $this->view->setHeaderCssInclude(Session::get('current_view'),'/public/ext/daterangepicker/daterangepicker.css');
            $this->view->renderHeader();
            $this->view->renderView('audit/view/index');
            $this->view->renderFooter();
            $this->view->setFooterJsInclude(Session::get('current_view'),'/public/ext/daterangepicker/moment.min.js');
            $this->view->setFooterJsInclude(Session::get('current_view'),'/public/ext/daterangepicker/daterangepicker.js');
            $this->view->renderScripts();
            $this->view->renderModuleJs('audit/view/js/index.js.php');
            $this->view->renderBottom();
        }
    }

}
