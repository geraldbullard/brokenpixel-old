<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: index.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/audit/view/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<div class="row">
    <div class="col-lg-12">
        <div class="box box-info filter-box<?php
            echo (isset($_GET['audit_filter']) && $_GET != '') ? '' : ' collapsed-box';
            ?>">
            <div class="box-header with-border">
                <h3 class="box-title">Search Filter</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                </div>
            </div>
            <form class="form-horizontal" action="/audit/" method="get">
                <input type="hidden" name="audit_filter" value="1" />
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="audit_user" class="col-sm-3 control-label">User</label>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <select
                                            class="form-control"
                                            id="audit_user"
                                            name="audit_user">
                                            <option value="">Select a User</option>
                                            <?php
                                            foreach (Users::loadAll() as $i => $user) {
                                                echo '<option value="' . $user['id'] . '"' .
                                                    ((isset($_GET['audit_user']) && $_GET['audit_user'] != '') ?
                                                        (
                                                            $_GET['audit_user'] == $user['id'] ? ' selected' : ''
                                                        ) : ''
                                                    )
                                                    . '>' . $user['firstname'] . ' ' .
                                                    $user['lastname'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="audit_date_range" class="col-sm-3 control-label">Date(s)</label>
                                <div class="col-sm-9">
                                    <div class="input-group filter-date-range">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input
                                            class="form-control pull-right"
                                            id="audit_date_range"
                                            name="audit_date_range"
                                            type="text"
                                            value="<?php
                                            echo (
                                                (isset($_GET['audit_date_range']) && $_GET['audit_date_range'] != '') ?
                                                urldecode($_GET['audit_date_range']) : ''
                                            );
                                            ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="audit_module" class="col-sm-3 control-label">Module</label>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <select
                                            id="audit_module"
                                            name="audit_module"
                                            class="form-control">
                                            <option value="">Select a Module</option>
                                            <?php
                                                foreach ($this->modulesArr as $module) {
                                                    echo '<option value="' . $module['module'] . '"' .
                                                        ((isset($_GET['audit_module']) && $_GET['audit_module'] != '')
                                                        ? ($_GET['audit_module'] == $module['module'] ? ' selected' : '')
                                                        : '') .
                                                    '>' .
                                                    $module['title'] . '</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <a href="/audit/"><button type="button" class="btn btn-default">Reset</button></a>
                    <button type="submit" class="btn btn-info pull-right">Filter</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body no-padding">
                <table class="table">
                    <tr>
                        <th class="audit-user-heading"><?php echo $Language->get('text_user'); ?></th>
                        <th class="hide-below-480"><?php echo $Language->get('text_action'); ?></th>
                        <th class="hide-below-1024"><?php echo $Language->get('text_date') .  '/' . $Language->get('text_time'); ?></th>
                        <th class="hide-below-1024"><?php echo $Language->get('text_url'); ?></th>
                        <th class="text-right"><?php echo $Language->get('text_related') .  ' ' . $Language->get('text_id'); ?></th>
                    </tr>
                    <?php
                    foreach ($this->auditList as $key => $value) {
                        echo '<tr>';
                        echo '  <input type="hidden" name="audit_id" value="' . $value['id'] . '">';
                        echo '  <td>' .
                            Users::getFullName($value['users_id']) . '&nbsp;&nbsp;&nbsp;' .
                            '<a 
                                href="javascript:;" 
                                onclick="$(\'#audit_info_' . $value['id'] . '\').toggle();" 
                                title="Click for more info">' .
                            '<i class="fa fa-caret-square-o-down"></i></a>' .
                            '<div style="display:none;" id="audit_info_' . $value['id'] . '">' .
                            '    <small>Agent: ' . $value['user_agent'] . '</small><br />' .
                            '    <small>IP: ' . $value['remote_host'] . '</small>' .
                            '</div>';
                            '</td>';
                        echo '  <td class="hide-below-480">' . $value['message'] . '</td>';
                        echo '  <td class="hide-below-1024">' . date("m-d-Y / h:i:s A", strtotime($value['timestamp'])) . '</td>';
                        echo '  <td class="hide-below-1024">' . $value['url'] . '</td>';
                        echo '  <td><div class="pull-right btn-group">' .
                            '<a href="/' . $value['url'] . $value['related_id'] . '">' .
                            '<button style="min-width:85px;" class="btn btn-xs btn-default">' .
                            ucfirst($value['module']) . ' ' .
                             $value['related_id'] .
                            '</button>' .
                            '</a>' .
                            '</div></td>';
                        echo '</tr>';
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="displaying-div">
            <?php echo Utils::returnDisplayingRowInfo($this->rpp, $this->page, $this->auditListResults); ?>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="pagination-div">
            <?php echo Utils::returnPagination(
                @$this->params, $this->totalPages, $this->page, false, $this->auditListResults
            ); ?>
        </div>
    </div>
</div>
