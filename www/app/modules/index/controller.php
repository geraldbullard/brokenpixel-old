<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: controller.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/index/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class index extends Controller {

    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        // view the site index page(s)
        $this->view->getTheme('site');
        $this->view->renderHeader();
        $this->view->renderView('index/view/index');
        $this->view->renderFooter();
        $this->view->renderScripts();
        $this->view->renderBottom();
    }

}
