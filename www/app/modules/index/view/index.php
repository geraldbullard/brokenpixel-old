<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: index.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/index/view/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
/*echo '<pre>';
print_r($Modules[$_SESSION['current_view']]['module']);
echo '</pre>';*/
?>
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <h1><?php echo $this->getContent()['title']; ?></h1>
                    </div>
                </div>
                <?php echo $this->getContent()['content']; ?>
            </div>
        </div>
