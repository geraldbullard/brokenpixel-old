<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: menu.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/content/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

$_menu = array(
    'item' => '/content',
    'title' => 'Content',
    'icon' => 'fa-file-text-o',
    'system' => 1,
    'sub_items' => array(
        'sub_item' => array(
            'item' => '/content/createcontent/',
            'title' => 'Create Content',
            'icon' => 'fa-plus',
            'system' => 1,
        ),
    ),
);
