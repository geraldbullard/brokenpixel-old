<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: index.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/content/view/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
    // Utils::adminURI($_GET['url'], 2)
?>
<!--<style>
    body.dragging, body.dragging * {
        cursor: move !important
    }
    .dragged {
        position: absolute;
        top: 0;
        opacity: .5;
        z-index: 2000;
    }
    ul.vertical {
        margin: 0;
        padding: 0;
        min-height: 10px;
    }
    ul.vertical li {
        display: block;
        margin: 5px 5px 0 5px;
        padding: 5px 5px 5px 5px;
        color: #000;
    }
    ul.vertical li .fa-arrows {
        cursor: pointer;
    }
    ul.vertical li.placeholder {
        position: relative;
        margin: 0;
        padding: 0;
        border: none;
    }
    ul.vertical li.placeholder:before {
        position: absolute;
        content: "";
        width: 0;
        height: 0;
        margin-top: -5px;
        left: -5px;
        top: -4px;
        border: 5px solid transparent;
        border-left-color: #ff0000;
        border-right: none;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body no-padding">
                <ul class="serialization vertical table">
                    <li data-id="1" data-name="Item 1">
                        <i class="fa fa-fw fa-arrows"></i> Item 1
                        <ol></ol>
                    </li>
                    <li data-id="2" data-name="Item 2">
                        <i class="fa fa-fw fa-arrows"></i> Item 2
                        <ol></ol>
                    </li>
                    <li data-id="3" data-name="Item 3">
                        <i class="fa fa-fw fa-arrows"></i> Item 3
                        <ol></ol>
                    </li>
                    <li data-id="4" data-name="Item 4">
                        <i class="fa fa-fw fa-arrows"></i> Item 4
                        <ol></ol>
                        <ol>
                            <li data-id="5" data-name="Item 5"><i class="fa fa-fw fa-arrows"></i> Item 5
                                <ol></ol></li>
                            <li data-id="11" data-name="Item 11"><i class="fa fa-fw fa-arrows"></i> Item 11
                                <ol></ol></li>
                        </ol>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>-->
<?php if (isset($_GET['parent']) && $_GET['parent'] != '') { ?>
<div class="row back-btn-parents">
    <div class="col-lg-12">
        <div class="back-btn-parents-inner">
            <?php
                echo '<a href="/content/' .
                (
                    content_Model::getDirectParent($_GET['parent'])['parent'] != 0 ?
                    '?parent=' . content_Model::getDirectParent($_GET['parent'])['parent']
                    : null
                ) . '">
                <button class="btn btn-info back-button" type="button">
                    <i class="fa fa-chevron-left"></i>
                    &nbsp;' . $Language->get('text_back') . '
                </button></a>';
                echo '<a href="/content/">' . $Language->get('text_top') . '</a>&nbsp; &raquo; &nbsp;';
                $parents = array_reverse(explode(",", content_Model::getParentsPath($_GET['parent'])));
                foreach ($parents as $parent) {
                    if (!empty($parent)) {
                        echo '<a href="/content/?parent=' . $parent . '">';
                        echo content_Model::getDirectParent($parent)['title'] . '</a>&nbsp; &raquo; &nbsp;';
                    }
                }
                echo content_Model::getDirectParent($_GET['parent'])['title'];
            ?>
        </div>
    </div>
</div>
<?php } ?>
<?php if (Session::getStatusMessage('contentMessage') != '') { ?>
<div class="row">
    <div class="col-lg-12" id="content-alert-message">
        <div class="alert alert-<?php echo Session::getStatusMessage('contentMessage')['status']; ?> alert-dismissible alert-message">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><?php echo $Language->get(Session::getStatusMessage('contentMessage')['value']); ?></h4>
        </div>
    </div>
</div>
<?php Session::setNull('contentMessage'); } ?>
<div class="row">
    <div class="col-xs-12 col-md-3 content-add-button">
        <a href="/content/createcontent<?php echo ($_GET['parent'] ? '?parent=' . $_GET['parent'] : null); ?>">
            <button class="btn btn-lg btn-primary" type="button">
                <i class="fa fa-plus"></i>
                &nbsp;<?php echo
                    $Language->get('text_add') . ' ' . $Language->get('text_new') .  ' ' . $Language->get('text_content');
                ?>
            </button>
        </a>
    </div>
    <div class="col-xs-12 col-md-9 pull-right" id="content-filter">
        <div class="box box-info filter-box<?php
        echo (isset($_GET['content_filter']) && $_GET['content_filter'] != '') ? '' : ' collapsed-box';
        ?>">
            <div class="box-header with-border">
                <h3 class="box-title">Search Filter</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-<?php
                        echo (isset($_GET['content_filter']) && $_GET['content_filter'] != '') ? 'minus' : 'plus';
                    ?>"></i></button>
                </div>
            </div>
            <form class="form-horizontal" action="/content/" method="get">
                <input type="hidden" name="content_filter" value="1" />
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="content_search" class="col-sm-3 control-label">Search</label>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="content_search"
                                            name="content_search"
                                            value="<?php
                                            echo (isset($_GET['content_search']) && $_GET['content_search'] != ''
                                                ? $_GET['content_search'] : '');
                                            ?>" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="content_type" class="col-sm-3 control-label">Type</label>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <select
                                            id="content_type"
                                            name="content_type"
                                            class="form-control">
                                            <option value="">Select a Type</option>
                                            <?php
                                            foreach ($ContentTypes as $type) {
                                                echo '<option value="' . $type->getId() . '"' . (
                                                    (isset($_GET['content_type']) && $_GET['content_type'] != '')
                                                    ? ($_GET['content_type'] == $type->getId() ? ' selected' : '')
                                                    : ''
                                                ) .
                                                '>' . $type->getTitle() . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="content_status" class="col-sm-3 control-label">Status</label>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <select
                                            id="content_status"
                                            name="content_status"
                                            class="form-control">
                                            <option value="">Select a Status</option>
                                            <option value="1"<?php
                                            echo (isset($_GET['content_status']) && $_GET['content_status'] == '1'
                                                ? ' selected' : ''); ?>>Active</option>
                                            <option value="0"<?php
                                            echo (isset($_GET['content_status']) && $_GET['content_status'] == '0'
                                                ? ' selected' : ''); ?>>Inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--<div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="content_category" class="col-sm-3 control-label">Category</label>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <select
                                            id="content_category"
                                            name="content_category"
                                            class="form-control">
                                            <option value="">Select a Category</option>
                                            <?php
                                            // pull in all content categories
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="content_date_range" class="col-sm-3 control-label">Publish Date(s)</label>
                                <div class="col-sm-9">
                                    <div class="input-group filter-date-range">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input
                                            class="form-control pull-right"
                                            id="content_date_range"
                                            name="content_date_range"
                                            type="text"
                                            value="<?php echo (
                                                (isset($_GET['content_date_range']) &&
                                                    $_GET['content_date_range'] != '') ?
                                                urldecode($_GET['content_date_range']) : ''
                                            ); ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>-->
                </div>
                <div class="box-footer">
                    <a href="/content/"><button type="button" class="btn btn-default">Reset</button></a>
                    <button type="submit" class="btn btn-info pull-right">Filter</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Sortable Content Entries Work
<div class="row">
    <div class="col-lg-12">
        <ol class="content-sortable">
            <li data-id="0" data-name="Item 1">
                Item 1
            </li>
            <li data-id="1" data-name="Item 2">
                Item 2
                <ol>
                    <li data-id="2-0" data-name="Item 2.1">Item 2.1</li>
                    <li data-id="2-1" data-name="Item 2.2">Item 2.2</li>
                </ol>
            </li>
        </ol>
        <textarea id="content-sortable-output"></textarea>
    </div>
</div>-->
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body no-padding">
                <table class="table sortable">
                    <thead>
                        <tr>
                            <th class="text-center hide-below-480" width="50"><?php echo $Language->get('text_sort'); ?></th>
                            <th class="text-center hide-below-480" width="60"><?php echo $Language->get('text_status'); ?></th>
                            <th class="text-center hide-below-768" width="80"><?php
                                echo $Language->get('text_site') . ' ' . $Language->get('text_index');
                            ?></th>
                            <th><?php echo $Language->get('text_title'); ?></th>
                            <th class="text-right actions-column">
                                <?php echo $Language->get('text_actions'); ?>&nbsp;&nbsp;
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($this->contentList as $key => $value) {
                            echo '<tr data-id="' . $value['id'] . '" data-parent="' . $value['parent'] . '" data-sort="' . $value['sort'] . '">';
                            echo '  <input type="hidden" name="content_id" value="' . $value['id'] . '">';
                            echo '  <td class="text-center content-sort hide-below-480">
                                <i class="fa fa-fw fa-arrows" title="' .
                                $Language->get('text_move_sort') . '"></i>
                            </td>';
                            echo '  <td class="text-center content-status hide-below-480">' .
                                ($value['status'] == 1 ?
                                    '<a href="/content/status/' . $value['id'] . '?status=0">' .
                                    '<i class="fa fa-check-circle fa-2x forestgreen" title="' .
                                    $Language->get('text_enabled') . ': ' .
                                    $Language->get('text_click_to_disable') . '"></i></a>' :
                                    '<a href="/content/status/' . $value['id'] . '?status=1">' .
                                    '<i class="fa fa-times-circle fa-2x red" title="' .
                                    $Language->get('text_disabled') . ': ' .
                                    $Language->get('text_click_to_enable') . '"></i></a>') .
                                '</td>';
                            echo '  <td class="text-center content-index hide-below-768">' .
                                ($value['siteIndex'] == 1 ?
                                    '<i class="fa fa-check-circle fa-2x forestgreen" title="' .
                                    $Language->get('text_existing_site_index') . '"></i>' :
                                    '<a href="/content/siteindex/' . $value['id'] . '">' .
                                    '<i class="fa fa-times-circle fa-2x red" title="' .
                                    $Language->get('text_set_site_index') . '"></i></a>') .
                                '</td>';
                            echo '  <td>' .
                                (
                                    content_Model::isParent($value['id']) ?
                                    '<a href="/content/?parent=' . $value['id'] . '" class="disable-sorting"><strong>'
                                    : null
                                ) .
                                '<i class="fa ' .
                                ContentTypes::loadById($value['type'])->getIcon() .
                                '" title="' . ContentTypes::loadById($value['type'])->getTitle() .
                                '"></i>&nbsp;&nbsp;' . $value['title'] .
                                (content_Model::isParent($value['id']) ? '</strong></a>' : null) . '</td>';
                            /*echo '  <td class="hide-below-768"><i class="fa ' .
                                ContentTypes::loadById($value['type'])->getIcon() .
                                '"></i> ' . ContentTypes::loadById($value['type'])->getTitle() . '</td>';*/
                            echo '<td>';
                            echo '<div class="pull-right btn-group"><a href="/' . $value['slug'] . '.html">
                                <button class="view-content btn btn-xs btn-success" type="button">
                                    <i class="fa fa-external-link"></i>
                                    <span class="hide-below-480">&nbsp;' . $Language->get('text_view') . '</span>
                                </button></a></div>';
                            echo '  <div class="pull-right btn-group">';
                            if (Session::get('content_access') >= ACCESS_EDIT) {
                                echo '    <button
                                    class="btn btn-xs btn-primary"
                                    type="button"
                                    onclick="location.href=\'/content/edit/' . $value['id'] . ($_GET['parent'] ? '?parent=' . $_GET['parent'] : null) . '\'">
                                    <i class="fa fa-pencil"></i>
                                    <span class="hide-below-480">&nbsp;' . $Language->get('text_edit') . '</span>
                                </button>';
                            }
                            if (Session::get('content_access') >= ACCESS_DELETE) {
                                echo '    <button
                                    class="delete-content btn btn-xs btn-danger"
                                    type="button"
                                    data-content-id="' . $value['id'] . '">
                                    <i class="fa fa-trash-o"></i>
                                    <span class="hide-below-480">&nbsp;' . $Language->get('text_delete') . '</span>
                                </button>';
                            }
                            echo '</div>';
                            echo '</td>';
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="displaying-div">
            <?php echo Utils::returnDisplayingRowInfo($this->rpp, $this->page, $this->contentListResults); ?>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="pagination-div">
            <?php echo Utils::returnPagination(@$this->params, $this->totalPages, $this->page, false, $this->contentListResults); ?>
        </div>
    </div>
</div>
