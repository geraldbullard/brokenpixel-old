<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: edit.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/user/view/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu, $Rci;
?>
<form method="post" action="/content/editsave/<?php echo $this->content['id']; ?>" id="contentsave">
    <input type="hidden" name="cur_parent" id="cur_parent" value="<?php echo $this->content['parent']; ?>" />
    <!-- RCI contenteditsidebar bottom -->
    <?php
        // Rci extend content edit sidebar
        echo $Rci->get('contenteditform', 'top', true);
    ?>
    <div class="row">
        <!-- Content Edit Main Panel -->
        <div class="col-md-8 col-lg-9 admin-edit-main">
            <!-- RCI contenteditmain top -->
            <?php
                // Rci extend content edit main
                echo $Rci->get('contenteditmain', 'top', true);
            ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <?php echo
                            ContentTypes::loadById($this->content['type'])->getTitle() . ': ' . $this->content['title'];
                        ?>
                    </h3>
                    <p class="panel-heading-view-page pull-right">
                        <a href="/<?php echo $this->content['slug']; ?>.html">
                            View Page&nbsp;<i class="fa fa-fw fa-external-link"></i></a>
                    </p>
                </div>
                <div class="panel-body">
                    <div class="col-lg-12">
                        <p>
                            <label for="title"><?php echo $Language->get('text_title'); ?></label>
                            <input type="text" name="title" id="title" class="form-control" value="<?php
                                echo $this->content['title'];
                            ?>">
                        </p>
                        <p>
                            <div class="pull-right" id="editor-fullscreen">
                                <?php echo $Language->get('text_full_screen'); ?>
                            </div>
                            <label for="content"><?php echo $Language->get('text_content'); ?></label>
                            <div id="content-editor" style="display:none;opacity: 0;"><?php
                                echo $this->content['content'];
                            ?></div>
                            <input type="hidden" name="content" id="hiddencontent" value="" />
                            <div id="overlay">
                                <img src="/public/themes/adminlte2/dist/img/loader.gif" alt="Loading" />
                            </div>
                        </p>
                        <p>
                            <label for="summary"><?php echo $Language->get('text_summary'); ?></label>
                            <input type="text" name="summary" id="summary" class="form-control" value="<?php
                            echo $this->content['summary'];
                            ?>">
                        </p>
                    </div>
                    <div class="col-lg-6">
                        <p>
                            <label for="type"><?php echo $Language->get('text_content_type'); ?></label>
                            <select name="type" id="type" class="form-control"><?php
                                foreach (ContentTypes::loadAll() as $t => $type) {
                                    echo '<option value="' . $type->getId() . '"' .
                                        ($type->getId() == $this->content['type'] ? ' selected' : '') . '>' .
                                        $type->getTitle() .
                                        '</option>';
                                }
                                ?>"</select>
                        </p>
                        <p>
                            <label for="metadescription"><?php echo $Language->get('text_meta_description'); ?></label>
                            <input type="text" name="metaDescription" id="metaDescription" class="form-control" value="<?php
                                echo $this->content['metaDescription'];
                            ?>">
                        </p>
                        <!--<p>
                            <label for="menutitle"><?php //echo $Language->get('text_menu_title'); ?></label>
                            <input type="text" name="menuTitle" id="menuTitle" class="form-control" value="<?php
                                //echo $this->content['menuTitle'];
                            ?>">
                        </p>
                        <p>
                            <label for="override"><?php //echo $Language->get('text_url_override'); ?></label>
                            <input type="text" name="override" id="override" class="form-control" value="<?php
                                //echo $this->content['override'];
                            ?>">
                        </p>
                        <p>
                            <label for="sort"><?php //echo $Language->get('text_sort'); ?></label>
                            <input type="text" name="sort" id="sort" class="form-control" value="<?php
                                //echo $this->content['sort'];
                            ?>">
                        </p>-->
                    </div>
                    <div class="col-lg-6">
                        <p>
                            <label for="slug"><?php echo $Language->get('text_slug'); ?></label>
                            <input type="text" name="slug" id="slug" class="form-control" value="<?php
                            echo $this->content['slug'];
                            ?>">
                        </p>
                        <p>
                            <label for="metakeywords"><?php echo $Language->get('text_meta_keywords'); ?></label>
                            <input type="text" name="metaKeywords" id="metaKeywords" class="form-control" value="<?php
                                echo $this->content['metaKeywords'];
                            ?>">
                        </p>
                        <!--<p>
                            <label for="botaction"><?php //echo $Language->get('text_bot_action'); ?></label>
                            <input type="text" name="botAction" id="botAction" class="form-control" value="<?php
                                //echo $this->content['botAction'];
                            ?>">
                        </p>
                        <p>
                            <label for="menu"><?php //echo $Language->get('text_in_menu'); ?></label>
                            <input type="text" name="menu" id="menu" class="form-control" value="<?php
                                //echo $this->content['menu'];
                            ?>">
                        </p>
                        <p>
                            <label for="categoryid"><?php //echo $Language->get('text_category_id'); ?></label>
                            <input type="text" name="categoryId" id="categoryId" class="form-control" value="<?php
                                //echo $this->content['categoryId'];
                            ?>">
                        </p>-->
                    </div>
                </div>
            </div>
            <!-- RCI contenteditmain bottom -->
            <?php
                // Rci extend content edit main
                echo $Rci->get('contenteditmain', 'bottom', true);
            ?>
        </div>
        <!-- Content Edit Sidebar Panel -->
        <div class="col-md-4 col-lg-3 admin-edit-sidebar">
            <!-- Publish Box -->
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo $Language->get('text_publish'); ?></h3>
                    <!--<div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>-->
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="content-sidebar-row content-sidebar-status">
                                <i class="fa fa-map-pin"></i> <?php echo $Language->get('text_status'); ?>:
                                <span class="admin-sidebar-bold"><?php
                                    echo ($this->content['status'] == 1 ? $Language->get('text_enabled') : $Language->get('text_disabled'))
                                ?></span><!-- <a href="javascript:showContentSidebarStatusEdit();">
                                    <?php //echo $Language->get('text_edit'); ?></a>
                                <div class="content-sidebar-status-edit">
                                    <p>
                                        <select name="status" id="content_status" class="form-control pull-left">
                                            <option value="1"<?php //echo ($this->content['status'] == 1 ? ' selected' : null); ?>>
                                                <?php //echo $Language->get('text_enabled'); ?></option>
                                            <option value="0"<?php //echo ($this->content['status'] == 0 ? ' selected' : null); ?>>
                                                <?php //echo $Language->get('text_disabled'); ?></option>
                                        </select>
                                        <button type="button" class="pull-left btn btn-md"
                                            onclick="showContentSidebarStatusEdit();">OK</button>
                                    </p>
                                </div>-->
                            </div>
                            <!--<div class="content-sidebar-row content-sidebar-visibility clear-both">
                                <i class="fa fa-eye"></i> <?php //echo $Language->get('text_visibility'); ?>:
                                <span class="admin-sidebar-bold"><?php
                                    //echo ($this->content['status'] == 1 ? $Language->get('text_enabled') : $Language->get('text_disabled'))
                                ?></span> <a href="javascript:showContentSidebarVisibilityEdit();">
                                    <?php //echo $Language->get('text_edit'); ?></a>
                                <div class="content-sidebar-visibility-edit">
                                    <p>Edit Content Visibility Div</p>
                                </div>
                            </div>-->
                            <!--<div class="content-sidebar-row content-sidebar-revisions clear-both">
                                <i class="fa fa-history"></i> <?php //echo $Language->get('text_revisions'); ?>:
                                <span class="admin-sidebar-bold"><?php
                                    //echo ($this->content['status'] == 1 ? $Language->get('text_enabled') : $Language->get('text_disabled'))
                                ?></span> <a href="javascript:showContentSidebarRevisionsEdit();">
                                    <?php //echo $Language->get('text_edit'); ?></a>
                                <div class="content-sidebar-revisions-edit">
                                    <p>Edit Content Revisions Div</p>
                                </div>
                            </div>-->
                            <div class="content-sidebar-row content-sidebar-info clear-both">
                                <i class="fa fa-calendar"></i> <?php echo $Language->get('text_published'); ?>:
                                <span class="admin-sidebar-bold"><?php
                                    echo $this->content['publicationDate']
                                ?></span>
                            </div>
                            <div class="content-sidebar-row content-sidebar-modified clear-both">
                                <i class="fa fa-calendar"></i> <?php echo $Language->get('text_last_modified'); ?>:
                                <span class="admin-sidebar-bold"><?php
                                    echo $this->content['lastModified']
                                ?></span>
                            </div>
                        </div>
                    </div>
                    <hr class="admin-box-footer" />
                    <div class="row">
                        <div class="col-lg-12">
                            <button type="button" class="cancel pull-left btn btn-sm btn-default">
				                <?php echo $Language->get('text_cancel'); ?>
                            </button>
                            <button type="button" class="pull-right btn btn-sm btn-danger save">
		                        <?php echo $Language->get('text_save'); ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Attributes Box -->
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo $Language->get('text_page_attributes'); ?></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="content-sidebar-row content-sidebar-modified clear-both">
                                <label for="parent"><?php echo $Language->get('text_parent'); ?></label>
                                <select id="parent_select" name="parent" class="form-control">
                                    <option value="0"><?php echo $Language->get('text_top'); ?></option>
                                    <?php echo content_Model::contentParentSelect(0, 1, $this->content['id']); ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- RCI contenteditsidebar bottom -->
            <?php
                // Rci extend content edit sidebar
                echo $Rci->get('contenteditsidebar', 'bottom', true);
            ?>
        </div>
    </div>
    <!-- RCI contenteditsidebar bottom -->
    <?php
        // Rci extend content edit sidebar
        echo $Rci->get('contenteditform', 'bottom', true);
    ?>
</form>
