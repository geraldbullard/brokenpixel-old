<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: create-edit.js.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/user/view/js/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<script>
    function showContentSidebarStatusEdit() {
        $(".content-sidebar-status-edit").toggle(400);
        $(".content-sidebar-visibility-edit").hide(400);
        $(".content-sidebar-revisions-edit").hide(400);
    }
    function showContentSidebarVisibilityEdit() {
        $(".content-sidebar-visibility-edit").toggle(400);
        $(".content-sidebar-revisions-edit").hide(400);
        $(".content-sidebar-status-edit").hide(400);
    }
    function showContentSidebarRevisionsEdit() {
        $(".content-sidebar-revisions-edit").toggle(400);
        $(".content-sidebar-visibility-edit").hide(400);
        $(".content-sidebar-status-edit").hide(400);
    }
    /*function checkForColSwap() {
        if (Utils.viewportWidth() < 992) {
            Utils.swapElements('.admin-edit-main', '.admin-edit-sidebar');
        } else if (Utils.viewportWidth() > 991) {
            if ($('.admin-edit-main').index() > 0) Utils.swapElements('.admin-edit-sidebar', '.admin-edit-main');
        }
    }*/
    $(document).ready(function () {
        $('#content-editor').slideDown(1000);
        $('#content-editor').fadeTo(1000, 1);

        // Initialize grid editor
        $('#content-editor').gridEditor({
            new_row_layouts: [
                [12],
                [4, 8],
                [6, 6],
                [8, 4],
                [3, 3, 3, 3],
                [4, 4, 4]
            ],
            content_types: ['ckeditor'],
            ckeditor: {
                config: {
                    on: {
                        instanceReady: function(evt) {
                            var instance = this;
                            //console.log('instance ready', evt, instance);
                        }
                    }
                }
            }
        });

        // Get resulting html
        var html = $('#content-editor').gridEditor('getHtml');
        //console.log(html);

        // clean the editor code away from the actual content before posting
        $('.save').click(function(e) {
            e.preventDefault();
            $("#content-editor .ge-content-type-ckeditor").each(function() {
                var currentDivContent = $(this).html();
                $(this).replaceWith(currentDivContent);
            });
            $("#content-editor .ui-sortable").each(function() {
                $(this).removeClass("ui-sortable");
            });
            $("#content-editor .column").each(function() {
                $(this).removeClass("column");
            });
            $('#content-editor').find("div.ge-tools-drawer").remove();
            var content = $('#content-editor').html();
            $('#hiddencontent').val(content);
            $("#contentsave").submit();
        });

        /*checkForColSwap();
        $(window).on('resize', function(){
            checkForColSwap();
        });*/

        $("#title").blur(function(){
            $("#slug").val($("#title").val().toLowerCase().replace(/ /g, '-').replace(/[^a-z0-9-]/g, ''));
        });
    });
    $(".cancel").confirm({
        title: "<?php echo $Language->get('text_cancel'); ?>",
        content: "<?php echo $Language->get('text_cancel_confirm'); ?>",
        buttons: {
            ok: function() {
                location.href = '/content/<?php echo ($_GET['parent'] ? '?parent=' . $_GET['parent'] : null); ?>';
            },
            cancel: function() {
            }
        }
    });
    // added for content editor fullscreen switch
    $("#editor-fullscreen").on('click', function(){
        $("#content-editor").addClass("fullscreen-overlay");
    });
    $("#parent_select > option").each(function() {
        if (this.value == $("#cur_parent").val()) {
            $(this).attr("selected", "selected");
        }
    });
</script>
