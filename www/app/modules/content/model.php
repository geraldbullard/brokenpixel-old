<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: model.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/content/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class content_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    public function contentList($params = '', $limit = '') {
        $where = array();
        $arr = array();
        $termsLike = array();
        if (@$_GET['content_search'] != '') {
            foreach (explode(" ", $_GET['content_search']) as $term) {
                $termsLike[] = "(`title` LIKE '%" . $term . "%' OR `menuTitle` LIKE '%" . $term . "%')";
            }
            $arr[] = implode(' AND ', $termsLike);
        }
        if (!empty($arr))
            $where[] = implode(" ", $arr);
        if (isset($_GET['content_type']) && $_GET['content_type'] != '')
            $where[] = ' `type` = ' . $_GET['content_type'];
        //if (isset($_GET['content_author']) && $_GET['content_author'] != '')
            //$where[] = ' `content_author` = ' . $_GET['content_author'];
        //if (isset($_GET['content_category']) && $_GET['content_category'] != '')
            //$where[] = ' `categoryId` = ' . $_GET['content_category'];
        //if (isset($_GET['content_date_range']) && $_GET['content_date_range'] != '')
            //$where[] = ' `publicationDate` = ' . $_GET['content_date_range'];
        if (isset($_GET['content_status']) && $_GET['content_status'] != '')
            $where[] = ' `status` = ' . $_GET['content_status'];
        if (isset($_GET['parent']) && $_GET['parent'] != '') {
            $where[] = ' `parent` = ' . $_GET['parent'];
        } else {
            $where[] = ' `parent` = 0';
        }
        if (!empty($where)) {
            $where = ' WHERE ' . implode(" AND ", $where);
        } else {
            $where = '';
        }
        if ($limit != '') $limit = ' LIMIT ' . $limit;
        return $this->db->select(
            "SELECT *
            FROM " . DB_PREFIX . "content " . $where . " ORDER BY `sort` ASC " . $limit
        );
    }

    public function contentSingleList($id) {
        $content =  $this->db->select(
            'SELECT * FROM ' . DB_PREFIX . 'content WHERE ' . DB_PREFIX . 'content.id = :id',
            array(':id' => $id)
        );
        return $content[0];
    }

    public function create($data) {
        $result = $this->db->insert(DB_PREFIX . 'content', array(
            'type' => $data['type'],
            'title' => $data['title'],
            'slug' => $data['slug'],
            'parent' => ($data['parent'] ? $data['parent'] : 0),
            'sort' => ($data['sort'] ? $data['sort'] : 0),
            'summary' => $data['summary'],
            'content' => $data['content'],
            'metaDescription' => $data['metaDescription'],
            'metaKeywords' => $data['metaKeywords'],
            'publicationDate' => date("Y-m-d h:i:s"),
            'lastModified' => date("Y-m-d h:i:s")
        ));
        if ($result) {
            Session::setStatusMessage('contentMessage', 'text_content_save_success_' . $data['type'], 'success');
            // insert the autdit trail
            AuditTrail::log(
                AuditTrail::RECORD_EDITED,
                Session::get('usersData'),
                'content/create/',
                'Content Created', // AuditTrail::loadTypeById(AuditTrail::RECORD_EDITED)->getTitle()
                $data['id']
            );
            return true;
        } else { // otherwise there was an error
            Session::setStatusMessage('contentMessage', $this->db->getError(), 'danger');
            return false;
        }
    }

    public function editSave($data) {
        global $postData, $Rci;
        $postData = array(
            'type' => $data['type'],
            'title' => $data['title'],
            'slug' => $data['slug'],
            'parent' => ($data['parent'] ? $data['parent'] : 0),
            'summary' => $data['summary'],
            'content' => $data['content'],
            'metaDescription' => $data['metaDescription'],
            'metaKeywords' => $data['metaKeywords'],
            'lastModified' => date("Y-m-d h:i:s")
        );
        // just in case we have an RCI module, let it adjust the $_POST values or other functionality if necessary
        echo $Rci->get('contentmodeleditsave', 'post', false);
        $result = $this->db->update(DB_PREFIX . 'content', $postData, "`id` = {$data['id']}");
        // add the success message to the session, and insert the audit trail db entry
        if ($result) {
            Session::setStatusMessage('contentMessage', 'text_content_save_success_' . $data['type'], 'success');
            // insert the autdit trail
            AuditTrail::log(
                AuditTrail::RECORD_EDITED,
                Session::get('usersData'),
                'content/edit/',
                'Content Edited', // AuditTrail::loadTypeById(AuditTrail::RECORD_EDITED)->getTitle()
                $data['id']
            );
            return true;
        } else { // otherwise there was an error
            Session::setStatusMessage('contentMessage', $this->db->errorInfo(), 'danger');
            return false;
        }
    }

    public function delete() {
        $parts = explode("/", $_GET['url']);
        $id = end($parts);
        $result = $this->db->delete(DB_PREFIX . "content", "`id` = {$id}");
        // add the success message to the session, and insert the audit trail db entry
        if ($result) {
            Session::setStatusMessage('contentMessage', 'text_content_delete_success', 'success');
            // insert the autdit trail
            AuditTrail::log(
                AuditTrail::RECORD_EDITED,
                Session::get('usersData'),
                'content/delete/',
                'Content Deleted', // AuditTrail::loadTypeById(AuditTrail::RECORD_EDITED)->getTitle()
                $id
            );
            return true;
        } else { // otherwise there was an error
            Session::setStatusMessage('contentMessage', $this->db->errorInfo(), 'danger');
            return false;
        }
    }

    public function siteIndex() {
    	$parts = explode("/", $_GET['url']);
    	$id = end($parts);
        $this->db->query("UPDATE `" . DB_PREFIX . "content` SET `siteIndex` = 0;");
        $result = $this->db->update(DB_PREFIX . "content", array('siteIndex' => 1), "`id` = {$id}");
	    // add the success message to the session, and insert the audit trail db entry
	    if ($result) {
		    Session::setStatusMessage('contentMessage', 'text_siteindex_save_success', 'success');
		    // insert the autdit trail
		    AuditTrail::log(
			    AuditTrail::RECORD_EDITED,
			    Session::get('usersData'),
			    'content/siteindex/',
			    'SiteIndex Updated', // AuditTrail::loadTypeById(AuditTrail::RECORD_EDITED)->getTitle()
			    $id
		    );
		    return true;
	    } else { // otherwise there was an error
		    Session::setStatusMessage('contentMessage', $this->db->errorInfo(), 'danger');
		    return false;
	    }
    }

    public function status() {
        $parts = explode("/", $_GET['url']);
    	$id = end($parts);
        $result = $this->db->update(DB_PREFIX . "content", array('status' => $_GET['status']), "`id` = {$id}");
	    // add the success message to the session, and insert the audit trail db entry
	    if ($result) {
            Session::setStatusMessage('contentMessage', 'text_content_status_save_success', 'success');
		    // insert the autdit trail
		    AuditTrail::log(
			    AuditTrail::RECORD_EDITED,
			    Session::get('usersData'),
			    'content/status/',
			    'Status Updated', // AuditTrail::loadTypeById(AuditTrail::RECORD_EDITED)->getTitle()
			    $id
		    );
		    return true;
	    } else { // otherwise there was an error
            Session::setStatusMessage('contentMessage', $this->db->errorInfo(), 'danger');
		    return false;
	    }
    }

    public static function isParent($id) {
        $conn = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
        $result =  $conn->select(
            "SELECT `id` FROM `" . DB_PREFIX . "content` WHERE `parent` = " . $id . " LIMIT 1"
        );
        return $result[0]['id'];
    }

    public static function getDirectParent($id) {
        $conn = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
        $result =  $conn->select(
            "SELECT `title`, `slug`, `parent` FROM `" . DB_PREFIX . "content` WHERE `id` = " . $id . " LIMIT 1"
        );
        return $result[0];
    }

    public static function getParentsPath($id = '', $parents = '') {
        $conn = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
        $result = self::getDirectParent($id);
        /*$parents_arr = array();
        $parents_arr = array_merge($parents, $parents_arr);
        $result = self::getDirectParent($id);
        $parents_arr[] = $result;
        if ($result != 0) {
            self::getParentsPath($result, $parents_arr);
        } else {
            $final = $parents_arr;
        }
        return $final;*/

        if ($result['parent'] != '0') {
            $add = $result['parent'] . ',';
            $parents .= self::getParentsPath($result['parent'], $add);
        }
        return $parents;
    }

    public static function contentParentSelect($parent = 0, $level = 0, $id = '') {
        global $Language;
        $conn = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
        $results = $conn->select("SELECT `id`, `title` FROM `" . DB_PREFIX . "content` WHERE `parent` = " . $parent . " AND `status` = 1 ORDER BY `sort` ASC");

        foreach ($results as $result) {
            if ($level == 0) {
                $add = '';
            } else {
                $add = str_repeat("&nbsp;&nbsp;&nbsp;&nbsp;", $level);
            }
            echo '<option value="' . $result['id'] . '"' . ($result['id'] == $id ? ' disabled' : null) . '>' . $add .
                '&raquo; ' . $result['title'] . ($result['id'] == $id ? ' (' . $Language->get('text_self') . ')' : null) . '</option>';
            self::contentParentSelect($result['id'], $level+1, $id);
        }
    }

}
