<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: controller.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/content/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class Content extends Controller {

    function __construct() {
        parent::__construct();
        // check if logged in
        if (Session::get('loggedIn') != true) {
            header('Location: /login/');
            exit;
        }
    }

    public function index() {
        // check for view access
        if (Session::get('content_access') < ACCESS_VIEW) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // START - added for pagination ////////////////////////////////////////////////////////////////////////////
            global $Options;
            $this->view->rpp = $Options['core']['contentAdminPagination'];
            if (@$_GET['page']) $page = intval(@$_GET['page']);
            if (@$page <= 0) $page = 1;
            $this->view->page = $page;
            $this->view->limit = (($page-1) * $this->view->rpp) . ',' . $this->view->rpp;
            if (isset($_GET['content_filter']) && $_GET['content_filter'] != '') {
                $this->view->params = array(
                    'content_filter' => 1,
                    'content_search' => @$_GET['content_search'],
                    'content_type' => @$_GET['content_type'],
                    'content_author' => @$_GET['content_author'],
                    'content_status' => @$_GET['content_status'],
                );
            }
            if (isset($_GET['content_filter']) && $_GET['content_filter'] != '') {
                $this->view->contentList = $this->model->contentList($this->view->params, $this->view->limit);
                $this->view->contentListResults = count($this->model->contentList($this->view->params));
            } else {
                $this->view->contentList = $this->model->contentList('', $this->view->limit);
                $this->view->contentListResults = count($this->model->contentList());
            }
            $this->view->totalPages = ceil($this->view->contentListResults/$this->view->rpp);
            // END - added for pagination //////////////////////////////////////////////////////////////////////////////
            // continue to content listing page if enough access
            $this->view->getTheme('admin');
            $this->view->heading = 'Content';
            $this->view->renderHeader();
            $this->view->renderView('content/view/index');
            $this->view->renderFooter();
            $this->view->setFooterJsInclude(Session::get('current_view'),'/public/themes/adminlte2/plugins/sortable/jquery-sortable.js');
            $this->view->renderScripts();
            $this->view->renderModuleJs('content/view/js/index.js.php');
            $this->view->renderBottom();
        }
    }

    public function create() {
        // check for create permissions
        if (Session::get('content_access') < ACCESS_CREATE) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // create content if enough permission
            $data = array();
            $data['title'] = $_POST['title'];
            $data['slug'] = $_POST['slug'];
            $data['parent'] = $_POST['parent'];
            $data['summary'] = $_POST['summary'];
            $data['content'] = $_POST['content'];
            $data['metaDescription'] = $_POST['metaDescription'];
            $data['metaKeywords'] = $_POST['metaKeywords'];
            $data['type'] = $_POST['type'];

            // @TODO: Do your error checking!
            $this->model->create($data);
            header('location: /content/' . ($_POST['cur_parent'] != '' ? '?parent=' . $_POST['cur_parent'] : null));
            exit;
        }
    }

    public function createContent() {
        // check for create permissions
        if (Session::get('content_access') < ACCESS_CREATE) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // continue to create content page if enough permission
            $this->view->getTheme('admin');
            $this->view->heading = 'Create New Content';
            $this->view->setHeaderCssInclude(Session::get('current_view'),'/public/ext/jquery-grideditor/grideditor.css');
            $this->view->renderHeader();
            $this->view->renderView('content/view/create');
            $this->view->renderFooter();
            $this->view->setFooterJsInclude(Session::get('current_view'),'/public/ext/jquery-grideditor/jquery.grideditor.js');
            $this->view->setFooterJsInclude(Session::get('current_view'),'//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.4/ckeditor.js');
            $this->view->renderScripts();
            $this->view->renderModuleJs('content/view/js/create-edit.js.php');
            $this->view->renderBottom();
        }
    }

    public function edit($id = '') {
    	// check for edit permissions
        if (Session::get('content_access') < ACCESS_EDIT) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // check if id is empty
            if (empty($id)) {
                // redirect if id is empty
                header('Location: /content/');
                exit;
            }
            // continue to content edit page if enough permission
            $this->view->content = $this->model->contentSingleList($id);
            $this->view->getTheme('admin');
            $this->view->heading = 'Edit Content';
            $this->view->setHeaderCssInclude(Session::get('current_view'),'/public/ext/jquery-grideditor/grideditor.css');
            $this->view->renderHeader();
            $this->view->renderView('content/view/edit');
            $this->view->renderFooter();
            $this->view->setFooterJsInclude(Session::get('current_view'),'/public/ext/jquery-grideditor/jquery.grideditor.js');
            $this->view->setFooterJsInclude(Session::get('current_view'),'//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.4/ckeditor.js');
            $this->view->renderScripts();
            $this->view->renderModuleJs('content/view/js/create-edit.js.php');
            $this->view->renderBottom();
        }
    }

    public function editSave($id = '') {
        // check for edit permissions
        if (Session::get('content_access') < ACCESS_EDIT) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // save content if enough permission
            $data = array();
            $data['id'] = $id;
            $data['title'] = $_POST['title'];
            $data['slug'] = $_POST['slug'];
            $data['parent'] = $_POST['parent'];
            $data['summary'] = $_POST['summary'];
            $data['content'] = $_POST['content'];
            $data['metaDescription'] = $_POST['metaDescription'];
            $data['metaKeywords'] = $_POST['metaKeywords'];
            $data['type'] = $_POST['type'];

            // @TODO: Do your error checking!
            $this->model->editSave($data);
            header('location: /content/' . ($_POST['cur_parent'] != '' ? '?parent=' . $_POST['cur_parent'] : null));
            exit;
        }
    }

    public function siteIndex() {
        // check for edit permissions
        if (Session::get('content_access') < ACCESS_EDIT) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // set siteIndex if enough permission
            // @TODO: Do your error checking!
            $this->model->siteIndex();
            header('location: /content/'/* . ($_POST['cur_parent'] != '' ? '?parent=' . $_POST['cur_parent'] : null)*/);
            exit;
        }
    }

    public function status() {
        // check for edit permissions
        if (Session::get('content_access') < ACCESS_EDIT) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // set status if enough permission
            // @TODO: Do your error checking!
            $this->model->status();
            header('location: /content/'/* . ($_POST['cur_parent'] != '' ? '?parent=' . $_POST['cur_parent'] : null)*/);
            exit;
        }
    }

    public function delete() {
        // check for edit permissions
        if (Session::get('content_access') < ACCESS_DELETE) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // delete content if enough permission
            // @TODO: Do your error checking!
            $this->model->delete();
            header('location: /content/'/* . ($_POST['cur_parent'] != '' ? '?parent=' . $_POST['cur_parent'] : null)*/);
            exit;
        }
    }

}
