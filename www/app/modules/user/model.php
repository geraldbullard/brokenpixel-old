<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: model.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/user/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class User_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    public function userList($params = '', $limit = '') {
        $where = '';
        $whereArr = array();
        $arr = array();
        $termsLike = array();
        if (@$_GET['user_search'] != '') {
            foreach (explode(" ", $_GET['user_search']) as $term) {
                $termsLike[] = "(`firstname` LIKE '%" . $term . "%' OR 
                    `lastname` LIKE '%" . $term . "%' OR 
                    `email` LIKE '%" . $term . "%' OR 
                    `username` LIKE '%" . $term . "%')";
            }
            $arr[] = implode(' AND ', $termsLike);
        }
        if (!empty($arr))
            $whereArr[] = implode(" ", $arr);
        if (isset($_GET['user_role']) && $_GET['user_role'] != '')
            $whereArr[] = ' `roles_id` = ' . $_GET['user_role'];
        if (isset($_GET['user_status']) && $_GET['user_status'] != '')
            $whereArr[] = ' `status` = ' . $_GET['user_status'];

        if (!empty($whereArr)) $where = ' WHERE ' . implode(" AND ", $whereArr);
        if ($limit != '') $limit = ' LIMIT ' . $limit;
        return $this->db->select(
            'SELECT *
            FROM ' . DB_PREFIX . 'users  
            LEFT JOIN ' . DB_PREFIX . 'roles 
            ON ' . DB_PREFIX . 'users.roles_id = ' . DB_PREFIX . 'roles.id ' . $where . $limit
        );
    }
    
    public function userSingleList($id) {
        return $this->db->select(
            'SELECT ' . DB_PREFIX . 'users.*, ' . DB_PREFIX . 'roles.name 
            FROM ' . DB_PREFIX . 'users 
            LEFT JOIN ' . DB_PREFIX . 'roles 
            ON ' . DB_PREFIX . 'users.roles_id = ' . DB_PREFIX . 'roles.id 
            WHERE ' . DB_PREFIX . 'users.id = :id',
            array(':id' => $id)
        );
    }
    
    public function create($data) {
        $this->db->insert(DB_PREFIX . 'users', array(
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'username' => $data['username'],
            'password' => Hash::create('sha256', $data['password'], HASH_PASSWORD_KEY),
            'gender' => $data['gender'],
            'roles_id' => $data['roles_id'],
            'status' => $data['status'],
            'created' => date("Y-m-d")
        ));
        // insert the autdit trail
        AuditTrail::log(
            AuditTrail::RECORD_CREATED,
            Session::get('usersData'),
            'user/create/',
            'User Created', // AuditTrail::loadTypeById(AuditTrail::RECORD_CREATED)->getTitle()
            $this->db->lastInsertId()
        );
        return $this->db->lastInsertId();
    }
    
    public function editSave($data) {
        if ($data['password'] != '') {
            $password = Hash::create('sha256', $data['password'], HASH_PASSWORD_KEY);
        } else {
        	$arr = self::userPassword($data['id']);
            $password = $arr[0]['password'];
        }
        $postData = array(
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'username' => $data['username'],
            'password' => $password,
            'roles_id' => $data['roles_id'],
            'status' => $data['status'],
            'gender' => $data['gender']
        );
        if ($this->db->update(DB_PREFIX . 'users', $postData, "`id` = " . $data['id'])) {
            // add the success message to the session, and insert the audit trail db entry
            Session::setStatusMessage('userMessage', 'text_user_save_success', 'success');
            // insert the autdit trail
	        AuditTrail::log(
	            AuditTrail::RECORD_EDITED,
	            Session::get('usersData'),
	            'user/edit/',
	            'User Edited', // AuditTrail::loadTypeById(AuditTrail::RECORD_EDITED)->getTitle()
	            $data['id']
	        );
	        return true;
        } else { // otherwise there was an error
            Session::setStatusMessage('userMessage', 'text_user_save_error', 'danger');
            return false;
        }
    }

    public function delete($id) {
        $result = $this->db->select(
            'SELECT roles_id 
            FROM ' . DB_PREFIX . 'users 
            WHERE id = :id',
            array(':id' => $id)
        );
        if ($result[0]['roles_id'] == '1') {
            return false;
        }
        $this->db->delete(DB_PREFIX . 'users', "id = '$id'");
        // insert the autdit trail
        AuditTrail::log(
            AuditTrail::RECORD_DELETED,
            Session::get('usersData'),
            'user/delete/',
            'User Deleted', // AuditTrail::loadTypeById(AuditTrail::RECORD_DELETED)->getTitle()
            $id
        );
    }

    public function getRoles() {
        return $this->db->select('SELECT * FROM ' . DB_PREFIX . 'roles');
    }

    public function getRoleName($roles_id) {
        $result = $this->db->select(
            'SELECT name 
            FROM ' . DB_PREFIX . 'roles 
            WHERE id = :id',
            array(':id' => $roles_id)
        );

        return $result[0]['name'];
    }

    public function userPassword($id) {
    	return $this->db->select(
            'SELECT password 
            FROM ' . DB_PREFIX . 'users 
            WHERE ' . DB_PREFIX . 'users.id = :id',
            array(':id' => $id)
        );
    }

}
