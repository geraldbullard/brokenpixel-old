<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: menu.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/user/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

$_menu = array(
    'item' => '/user',
    'title' => 'Users',
    'icon' => 'fa-users',
    'system' => 1,
    'sub_items' => array(
        'sub_item' => array(
            'item' => '/user/createuser/',
            'title' => 'Create User',
            'icon' => 'fa-plus',
            'system' => 1,
        ),
    ),
);
