<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: index.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/user/view/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<?php if (Session::getStatusMessage('userMessage') != '') { ?>
<div class="row">
    <div class="col-lg-12" id="content-alert-message">
        <div class="alert alert-<?php echo Session::getStatusMessage('userMessage')['status']; ?> alert-dismissible alert-message">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><?php echo $Language->get(Session::getStatusMessage('userMessage')['value']); ?></h4>
        </div>
    </div>
</div>
<?php Session::setNull('userMessage'); } ?>
<div class="row">
    <div class="col-xs-12 col-md-3 user-add-button">
        <a href="/user/createuser">
            <button class="btn btn-lg btn-primary" type="button">
                <i class="fa fa-plus"></i>
                &nbsp;<?php echo
                    $Language->get('text_add') . ' ' . $Language->get('text_new') .  ' ' . $Language->get('text_user');
                ?>
            </button>
        </a>
    </div>
    <div class="col-xs-12 col-md-9 pull-right" id="user-filter">
        <div class="box box-info filter-box<?php
        echo (isset($_GET['user_filter']) && $_GET != '') ? '' : ' collapsed-box';
        ?>">
            <div class="box-header with-border">
                <h3 class="box-title">Search Filter</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-<?php
                        echo (isset($_GET['user_filter']) && $_GET != '') ? 'minus' : 'plus';
                        ?>"></i></button>
                </div>
            </div>
            <form class="form-horizontal" action="/user/" method="get">
                <input type="hidden" name="user_filter" value="1" />
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="user_search" class="col-sm-3 control-label">Search</label>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="user_search"
                                            name="user_search"
                                            value="<?php
                                            echo (isset($_GET['user_search']) && $_GET['user_search'] != ''
                                                ? $_GET['user_search'] : '');
                                            ?>" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="user_role" class="col-sm-3 control-label">Role</label>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <select
                                            id="user_role"
                                            name="user_role"
                                            class="form-control">
                                            <option value="">Select a Role</option>
                                            <?php
                                            foreach ($this->userRolesArr as $role) {
                                                echo '<option value="' . $role['id'] . '"' .
                                                    ((isset($_GET['user_role']) && $_GET['user_role'] != '')
                                                        ? ($_GET['user_role'] == $role['id'] ? ' selected' : '')
                                                        : '') .
                                                    '>' .
                                                    $role['name'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="user_status" class="col-sm-3 control-label">Status</label>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <select
                                            id="user_status"
                                            name="user_status"
                                            class="form-control">
                                            <option value="">Select a Status</option>
                                            <option value="1"<?php
                                                echo (isset($_GET['user_status']) && $_GET['user_status'] == '1'
                                                ? ' selected' : ''); ?>>Active</option>
                                            <option value="0"<?php
                                                echo (isset($_GET['user_status']) && $_GET['user_status'] == '0'
                                                ? ' selected' : ''); ?>>Inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <a href="/user/"><button type="button" class="btn btn-default">Reset</button></a>
                    <button type="submit" class="btn btn-info pull-right">Filter</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body no-padding">
                <table class="table">
                    <tr>
                        <th class="hide-below-1024"><?php echo $Language->get('text_id'); ?></th>
                        <th><?php echo $Language->get('text_first') . ' ' . $Language->get('text_name'); ?></th>
                        <th><?php echo $Language->get('text_last') . ' ' . $Language->get('text_name'); ?></th>
                        <th class="hide-below-768"><?php echo $Language->get('text_email'); ?></th>
                        <th class="hide-below-1024"><?php echo $Language->get('text_username'); ?></th>
                        <th class="hide-below-1024"><?php echo $Language->get('text_role'); ?></th>
                        <th class="hide-below-1024"><?php echo $Language->get('text_status'); ?></th>
                        <th class="hide-below-1024"><?php echo $Language->get('text_created'); ?></th>
                        <th class="text-right actions-column"><?php echo $Language->get('text_actions'); ?></th>
                    </tr>
                    <?php
                    foreach ($this->userList as $key => $value) {
                        echo '<tr>';
                        echo '  <input type="hidden" name="roles_id" value="' . $value['roles_id'] . '">';
                        echo '  <td class="hide-below-1024">' . $value['id'] . '</td>';
                        echo '  <td>' . $value['firstname'] . '</td>';
                        echo '  <td>' . $value['lastname'] . '</td>';
                        echo '  <td class="hide-below-768">' . $value['email'] . '</td>';
                        echo '  <td class="hide-below-1024">' . $value['username'] . '</td>';
                        echo '  <td class="hide-below-1024">' . $value['name'] . '</td>';
                        echo '  <td class="hide-below-1024">' .
                            ($value['status'] == '1' ? $Language->get('text_active') : $Language->get('text_inactive'))
                            . '</td>';
                        $created = date_create($value['created']);
                        echo '  <td class="hide-below-1024">' . date_format($created, "M d, Y") . '</td>';
                        echo '  <td><div class="pull-right btn-group">';
                        if (Session::get('user_access') >= ACCESS_EDIT) {
                            echo '    <button
                                        class="btn btn-xs btn-primary"
                                        type="button"
                                        onclick="location.href=\'/user/edit/' . $value['id'] . '\'">
                                        <i class="fa fa-pencil"></i> <span class="hide-below-480">' . $Language->get('text_edit') . '</span>
                                      </button>';
                        }
                        if (Session::get('user_access') >= ACCESS_DELETE) {
                            echo '    <button
                                        class="delete-user btn btn-xs btn-danger"
                                        type="button"
                                        data-user-id="' . $value['id'] . '">
                                        <i class="fa fa-trash-o"></i> <span class="hide-below-480">' . $Language->get('text_delete') . '</span>
                                      </button>';
                        }
                        echo '  </div></td>';
                        echo '</tr>';
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="displaying-div">
            <?php echo Utils::returnDisplayingRowInfo($this->rpp, $this->page, $this->userListResults); ?>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="pagination-div">
            <?php
            echo Utils::returnPagination(
                @$this->params, $this->totalPages, $this->page, false, $this->userListResults
            );
            ?>
        </div>
    </div>
</div>
