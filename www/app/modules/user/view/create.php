<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: create.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/user/view/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<form method="post" action="/user/create" id="usercreate">
<div class="row">
    <div class="col-sm-12 col-md-8 col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php echo $Language->get('text_new') . ' ' . $Language->get('text_user') . ' ' . $Language->get('text_information'); ?>
                </h3>
            </div>
            <div class="panel-body">
                <p>
                    <label for="firstname"><?php echo $Language->get('text_first') . ' ' . $Language->get('text_name'); ?></label>
                    <input
                        type="text"
                        id="firstname"
                        name="firstname"
                        class="form-control" />
                </p>
                <p>
                    <label for="lastname"><?php echo $Language->get('text_last') . ' ' . $Language->get('text_name'); ?></label>
                    <input
                        type="text"
                        name="lastname"
                        class="form-control" />
                </p>
                <p>
                    <label for="email"><?php echo $Language->get('text_email'); ?></label>
                    <input
                        type="text"
                        name="email"
                        class="form-control" />
                </p>
                <p>
                    <label for="username"><?php echo $Language->get('text_username'); ?></label>
                    <input
                        type="text"
                        name="username"
                        class="form-control" />
                </p>
                <p>
                    <label for="password"><?php echo $Language->get('text_password'); ?></label>
                    <input
                        type="password"
                        name="password"
                        class="form-control" />
                </p>
                <p>
                    <label for="confirmpassword"><?php echo $Language->get('text_confirm') . ' ' . $Language->get('text_password'); ?></label>
                    <input
                        type="password"
                        name="confirmpassword"
                        class="form-control" />
                </p>
                <?php if (Session::get('loggedIn') == true) { ?>
                <p>
                    <label for="roles_id"><?php echo $Language->get('text_role'); ?></label>
                    <select name="roles_id" class="form-control">
                        <option value="">
                            <?php echo $Language->get('text_select') . ' ' . $Language->get('text_user') . ' ' . $Language->get('text_role'); ?>
                        </option>
                        <option value="1">
                            <?php echo $Language->get('text_owner'); ?>
                        </option>
                        <option value="2">
                            <?php echo $Language->get('text_admin'); ?>
                        </option>
                        <option value="3">
                            <?php echo $Language->get('text_user'); ?>
                        </option>
                    </select>
                </p>
                <p>
                    <label for="status"><?php echo $Language->get('text_status'); ?></label>
                    <select name="status" class="form-control">
                        <option value="">
                            <?php echo $Language->get('text_select') . ' ' . $Language->get('text_status'); ?>
                        </option>
                        <option value="1">
                            <?php echo $Language->get('text_active'); ?>
                        </option>
                        <option value="-1">
                            <?php echo $Language->get('text_inactive'); ?>
                        </option>
                    </select>
                </p>
                <p>
                    <label for="gender"><?php echo $Language->get('text_gender'); ?></label>
                    <select name="gender" class="form-control">
                        <option value="">
                            <?php echo $Language->get('text_select') . ' ' . $Language->get('text_gender'); ?>
                        </option>
                        <option value="1">
                            <?php echo $Language->get('text_male'); ?>
                        </option>
                        <option value="-1">
                            <?php echo $Language->get('text_female'); ?>
                        </option>
                    </select>
                </p>
                <?php } else { ?>
                <input type="hidden" name="roles_id" value="3">
                <input type="hidden" name="status" value="0">
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-md-8 col-lg-6">
        <button type="submit" class="pull-left btn btn-lg btn-danger">
            <?php echo $Language->get('text_create'); ?>
        </button>
        <button
            type="button"
            class="cancel pull-right btn btn-lg btn-default">
            <?php echo $Language->get('text_cancel'); ?>
        </button>
    </div>
</div>
</form>
