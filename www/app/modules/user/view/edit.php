<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: edit.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/user/view/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<form method="post" action="/user/editsave/<?php echo $this->user[0]['id']; ?>" id="usercreate">
<div class="row">
    <div class="col-sm-12 col-md-8 col-lg-6">
	    <?php if (isset($_GET['msg']) && $_GET['msg'] == 'edit_failed') { ?>
            <div class="edit-msg">
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="icon fa fa-check"></i> Danger alert preview. This alert is dismissable.
                </div>
            </div>
	    <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php echo $this->user[0]['firstname'] . ' ' . $this->user[0]['lastname']; ?>
                </h3>
            </div>
            <div class="panel-body">
                <p>
                    <label for="firstname"><?php echo $Language->get('text_first') . ' ' . $Language->get('text_name'); ?></label>
                    <input
                        type="text"
                        name="firstname"
                        id="firstname"
                        class="form-control"
                        value="<?php echo $this->user[0]['firstname']; ?>" />
                </p>
                <p>
                    <label for="lastname"><?php echo $Language->get('text_last') . ' ' . $Language->get('text_name'); ?></label>
                    <input
                        type="text"
                        name="lastname"
                        class="form-control"
                        value="<?php echo $this->user[0]['lastname']; ?>" />
                </p>
                <p>
                    <label for="email"><?php echo $Language->get('text_email'); ?></label>
                    <input
                        type="text"
                        name="email"
                        class="form-control"
                        value="<?php echo $this->user[0]['email']; ?>" />
                </p>
                <p>
                    <label for="username"><?php echo $Language->get('text_username'); ?></label>
                    <input
                        type="text"
                        name="username"
                        class="form-control"
                        value="<?php echo $this->user[0]['username']; ?>" />
                </p>
                <p>
                    <label for="password"><?php echo $Language->get('text_password'); ?></label>
                    <input
                        type="password"
                        name="password"
                        class="form-control"
                        autocomplete="off" />
                </p>
                <p>
                    <label for="confirmpassword"><?php echo $Language->get('text_confirm') . ' ' . $Language->get('text_password'); ?></label>
                    <input
                        type="password"
                        name="confirmpassword"
                        class="form-control"
                        autocomplete="off" />
                </p>
                <p>
                    <label for="roles_id"><?php echo $Language->get('text_role'); ?></label>
                    <select name="roles_id" class="form-control">
                        <option value="1" <?php echo ($this->user[0]['roles_id'] == '1') ? 'selected' : ''; ?>>
                            <?php echo $Language->get('text_developer'); ?>
                        </option>
                        <option value="2" <?php echo ($this->user[0]['roles_id'] == '2') ? 'selected' : ''; ?>>
                            <?php echo $Language->get('text_admin'); ?>
                        </option>
                        <option value="3" <?php echo ($this->user[0]['roles_id'] == '3') ? 'selected' : ''; ?>>
                            <?php echo $Language->get('text_user'); ?>
                        </option>
                    </select>
                </p>
                <p>
                    <label for="status"><?php echo $Language->get('text_status'); ?></label>
                    <select name="status" class="form-control">
                        <option value="1" <?php echo ($this->user[0]['status'] == '1') ? 'selected' : ''; ?>>
                            <?php echo $Language->get('text_active'); ?>
                        </option>
                        <option value="0" <?php echo ($this->user[0]['status'] == '0') ? 'selected' : ''; ?>>
                            <?php echo $Language->get('text_inactive'); ?>
                        </option>
                    </select>
                </p>
                <p>
                    <label for="gender"><?php echo $Language->get('text_gender'); ?></label>
                    <select name="gender" class="form-control">
                        <option value="m" <?php echo ($this->user[0]['gender'] == 'm') ? 'selected' : ''; ?>>
                            <?php echo $Language->get('text_male'); ?>
                        </option>
                        <option value="f" <?php echo ($this->user[0]['gender'] == 'f') ? 'selected' : ''; ?>>
                            <?php echo $Language->get('text_female'); ?>
                        </option>
                    </select>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-md-8 col-lg-6">
        <button type="submit" class="pull-left btn btn-lg btn-danger">
            <?php echo $Language->get('text_save'); ?>
        </button>
        <button
            type="button"
            class="cancel pull-right btn btn-lg btn-default">
            <?php echo $Language->get('text_cancel'); ?>
        </button>
    </div>
</div>
</form>
