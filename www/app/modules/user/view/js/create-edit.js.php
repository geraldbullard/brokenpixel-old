<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: create-edit.js.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/user/view/js/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<script>
    $(".cancel").confirm({
        title: "<?php echo $Language->get('text_cancel'); ?>",
        content: "<?php echo $Language->get('text_cancel_confirm'); ?>",
        buttons: {
            ok: function() {
                location.href = '/user/';
            },
            cancel: function() {
            }
        }
    });
    $(document).ready(function () {
        setTimeout(function() {
            $("#usercreate").trigger("reset");
            $("#firstname").focus();
        }, 700);
    });
</script>
