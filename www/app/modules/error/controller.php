<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: controller.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/error/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class Error extends Controller {

    function __construct() {
        parent::__construct(); 
    }
    
    function index() {
        // view the error page
        $this->view->getTheme('site');
        $this->view->renderHeader();
        $this->view->renderView('error/view/index');
        $this->view->renderFooter();
        $this->view->renderScripts();
        $this->view->renderBottom();
    }

}
