<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: index.js.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/options/view/js/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<script>
    $(".delete-option").confirm({
        title: "<?php echo $Language->get('text_delete') . ' ' . $Language->get('text_option'); ?>",
        content: "<?php echo $Language->get('text_options_delete_confirm'); ?>",
        buttons: {
            ok: function() {
                location.href = '/options/delete/' + this.$target.attr('data-option-id');
            },
            cancel: function() {
            }
        }
    });
    $(document).ready(function () {
        setTimeout(function() {
            $(".alert.alert-success.alert-dismissible").fadeOut("slow");
            $(".alert.alert-info.alert-dismissible").fadeOut("slow");
            $(".alert.alert-warning.alert-dismissible").fadeOut("slow");
            $(".alert.alert-danger.alert-dismissible").fadeOut("slow");
        }, 3000);
    });
</script>
