<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: index.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/options/view/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<?php if (Session::getStatusMessage('optionMessage') != '') { ?>
<div class="row">
    <div class="col-lg-12" id="content-alert-message">
        <div class="alert alert-<?php echo Session::getStatusMessage('optionMessage')['status']; ?> alert-dismissible alert-message">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><?php echo $Language->get(Session::getStatusMessage('optionMessage')['value']); ?></h4>
        </div>
    </div>
</div>
<?php Session::setNull('optionMessage'); } ?>
<div class="row">
    <div class="col-xs-12 col-md-3 option-add-button">
        <a href="/options/createoption">
            <button class="btn btn-lg btn-primary" type="button">
                <i class="fa fa-plus"></i>
                &nbsp;<?php echo
                    $Language->get('text_add') . ' ' . $Language->get('text_new') .  ' ' . $Language->get('text_option');
                ?>
            </button>
        </a>
    </div>
    <div class="col-xs-12 col-md-9 pull-right" id="options-filter">
        <div class="box box-info filter-box<?php
        echo (isset($_GET['options_filter']) && $_GET != '') ? '' : ' collapsed-box';
        ?>">
            <div class="box-header with-border">
                <h3 class="box-title">Search Filter</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-<?php
                        echo (isset($_GET['options_filter']) && $_GET != '') ? 'minus' : 'plus';
                        ?>"></i></button>
                </div>
            </div>
            <form class="form-horizontal" action="/options/" method="get">
                <input type="hidden" name="options_filter" value="1" />
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="options_search" class="col-sm-3 control-label">Search</label>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="options_search"
                                            name="options_search"
                                            value="<?php
                                                echo (isset($_GET['options_search']) && $_GET['options_search'] != ''
                                                ? $_GET['options_search'] : '');
                                            ?>" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="options_type" class="col-sm-3 control-label">Type</label>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <select
                                            id="options_type"
                                            name="options_type"
                                            class="form-control">
                                            <option value="">Select a Type</option>
                                            <?php
                                            foreach ($this->optionsTypeArr as $type) {
                                                echo '<option value="' . $type['type'] . '"' .
                                                    ((isset($_GET['options_type']) && $_GET['options_type'] != '')
                                                        ? ($_GET['options_type'] == $type['type'] ? ' selected' : '')
                                                        : '') .
                                                    '>' .
                                                    $type['title'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <a href="/options/"><button type="button" class="btn btn-default">Reset</button></a>
                    <button type="submit" class="btn btn-info pull-right">Filter</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="box listing-box">
            <div class="box-body no-padding">
                <table class="table">
                    <tr>
                        <th class="hide-below-1024"><?php echo $Language->get('text_id'); ?></th>
                        <th class="hide-below-1024"><?php echo $Language->get('text_define'); ?></th>
                        <th><?php echo $Language->get('text_title'); ?></th>
                        <th class="hide-below-1024"><?php echo $Language->get('text_summary'); ?></th>
                        <th class="hide-below-480"><?php echo $Language->get('text_value'); ?></th>
                        <!--<th><?php echo $Language->get('text_type'); ?></th>
                        <th><?php echo $Language->get('text_edit'); ?></th>
                        <th><?php echo $Language->get('text_system'); ?></th>-->
                        <th class="text-right actions-column"><?php echo $Language->get('text_actions'); ?></th>
                    </tr>
                    <?php
                    foreach ($this->optionsList as $key => $value) {
                        echo '<tr>';
                        echo '  <input type="hidden" name="options_id" value="' . $value['id'] . '">';
                        echo '  <td class="hide-below-1024">' . $value['id'] . '</td>';
                        echo '  <td class="hide-below-1024">' . $value['define'] . '</td>';
                        echo '  <td>' . $value['title'] . '</td>';
                        echo '  <td class="hide-below-1024">' . $value['summary'] . '</td>';
                        echo '  <td class="hide-below-480">' . $value['value'] . '</td>';
                        //echo '  <td>' . $value['type'] . '</td>';
                        //echo '  <td>' . $value['edit'] . '</td>';
                        //echo '  <td>' . $value['system'] . '</td>';
                        echo '  <td><div class="pull-right btn-group">';
                        if (Session::get('options_access') >= ACCESS_EDIT) {
                            $page = (
                                isset($_GET['page']) && $_GET['page'] != '' ? '?page=' . $_GET['page'] : ''
                            );
                            echo '    <button
                                class="btn btn-xs btn-primary"
                                type="button"
                                onclick="location.href=\'/options/edit/' . $value['id'] . '/' . $page . '\'">
                                <i class="fa fa-pencil"></i> 
                                &nbsp;<span class="hide-below-480">' . $Language->get('text_edit') . '</span>
                            </button>';
                        }
                        if (Session::get('options_access') >= ACCESS_DELETE) {
                            echo '    <button
                                class="delete-option btn btn-xs btn-danger"
                                type="button"
                                ' . ($value['system'] == 1 ? ' disabled' : NULL) . '
                                data-option-id="' . $value['id'] . '">
                                <i class="fa fa-trash-o"></i> 
                                &nbsp;<span class="hide-below-480">' . $Language->get('text_delete') . '</span>
                            </button>';
                        }
                        echo '  </div></td>';
                        echo '</tr>';
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="displaying-div">
            <?php echo Utils::returnDisplayingRowInfo($this->rpp, $this->page, $this->optionsListResults); ?>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="pagination-div">
            <?php echo Utils::returnPagination(@$this->params, $this->totalPages, $this->page, false, $this->optionsListResults); ?>
        </div>
    </div>
</div>
