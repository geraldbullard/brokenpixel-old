<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: menu.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/options/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

$_menu = array(
    'item' => '/options',
    'title' => 'Options',
    'icon' => 'fa-cogs',
    'system' => 1,
    'sub_items' => array(
        'sub_item' => array(
            'item' => '/options/createoption/',
            'title' => 'Create Option',
            'icon' => 'fa-plus',
            'system' => 1,
        ),
    ),
);
