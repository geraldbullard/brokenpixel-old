<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: controller.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/options/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class Options extends Controller {

    function __construct() {
        parent::__construct();
        // check if logged in
        if (Session::get('loggedIn') != true) {
            // redirect if not logged in
            header('Location: /noaccess/');
            exit;
        }
    }
    
    function index() {
        // check for view permissions
        if (Session::get('options_access') < ACCESS_VIEW) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // START - added for pagination ////////////////////////////////////////////////////////////////////////////
            global $Options;
            $this->view->rpp = $Options['core']['optionsAdminPagination'];
            if (@$_GET['page']) $page = intval(@$_GET['page']);
            if (@$page <= 0) $page = 1;
            $this->view->page = $page;
            $this->view->limit = (($page-1) * $this->view->rpp) . ',' . $this->view->rpp;
            // START Search Filter
            $this->view->optionsTypeArr = array();
            foreach (Option::loadAllTypes() as $t => $type) {
                $this->view->optionsTypeArr[$t] = array(
                    'type' => $type->getCode(),
                    'title' => $type->getTitle(),
                );
            }
            // added for pagination
            if (isset($_GET['options_filter']) && $_GET['options_filter'] != '') {
                $this->view->params = array(
                    'options_filter' => 1,
                    'options_search' => $_GET['options_search'],
                    'options_type' => $_GET['options_type'],
                );
            }
            // continue to options listing page if enough permission
            if (isset($_GET['options_filter']) && $_GET['options_filter'] != '') {
                $this->view->optionsList = $this->model->optionsList(@$_GET['options_type'], $this->view->limit);
                $this->view->optionsListResults = count($this->model->optionsList(@$_GET['options_type']));
            } else {
                $this->view->optionsList = $this->model->optionsList('', $this->view->limit);
                $this->view->optionsListResults = count($this->model->optionsList());
            }
            $this->view->totalPages = ceil($this->view->optionsListResults/$this->view->rpp);
            // END - added for pagination //////////////////////////////////////////////////////////////////////////////
            $this->view->getTheme('admin');
            $this->view->heading = 'Options';
            $this->view->renderHeader();
            $this->view->renderView('options/view/index');
            $this->view->renderFooter();
            $this->view->renderScripts();
            $this->view->renderModuleJs('options/view/js/index.js.php');
            $this->view->renderBottom();
        }
    }

    public function createOption() {
        // check for create permissions
        if (Session::get('options_access') < ACCESS_CREATE) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // continue to create user page if enough permission
            $this->view->getTheme('admin');
            $this->view->heading = 'Create New Option';
            $this->view->renderHeader();
            $this->view->renderView('options/view/create');
            $this->view->renderFooter();
            $this->view->renderScripts();
            $this->view->renderModuleJs('options/view/js/create-edit.js.php');
            $this->view->renderBottom();
        }
    }

    public function create() {
        // check for create permissions
        if (Session::get('options_access') < ACCESS_CREATE) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // create option if enough permission
            $data = array();
            $data['define'] = $_POST['define'];
            $data['title'] = $_POST['title'];
            $data['summary'] = $_POST['summary'];
            $data['value'] = $_POST['value'];
            $data['type'] = $_POST['type'];
            $data['edit'] = $_POST['edit'];
            $data['system'] = $_POST['system'];

            if ($this->model->create($data)) {
                header('location: /options?msg=create_success');
                exit;
            } else {
                header('location: /options?msg=create_failed');
                exit;
            }
        }
    }

    public function edit($id = '') {
        // check for edit permissions
        if (Session::get('options_access') < ACCESS_EDIT) {
            // redirect if not enough permission
            header('Location: /noaccess/');
            exit;
        } else {
            // check if id is empty
            if (empty($id)) {
                // redirect if id empty
                header('Location: /options');
                exit;
            }
            // continue to edit option if enough permission
            $this->view->options = $this->model->optionsSingleList($id);
            $this->view->getTheme('admin');
            $this->view->heading = 'Edit Option';
            $this->view->renderHeader();
            $this->view->renderView('options/view/edit');
            $this->view->renderFooter();
            $this->view->renderScripts();
            $this->view->renderModuleJs('options/view/js/create-edit.js.php');
            $this->view->renderBottom();
        }
    }

    public function editSave($id = '') {
        // check for edit permissions
        if (Session::get('options_access') < ACCESS_EDIT) {
            // redirect if not enough permission
            header('location: /noaccess/');
            exit;
        } else {
            // save option if enough permission
            $data = array();
            $data['id'] = $id;
            $data['define'] = $_POST['define'];
            $data['title'] = $_POST['title'];
            $data['summary'] = $_POST['summary'];
            $data['value'] = $_POST['value'];
            $data['type'] = $_POST['type'];
            $data['edit'] = $_POST['edit'];
            $data['system'] = $_POST['system'];

            // @TODO: Do your error checking!

            $this->model->editSave($data);
            header(
                'location: /options/' . (
                    isset($_POST['page']) && $_POST['page'] != '' ? '?page=' . $_POST['page'] : ''
                )
            );
            exit;
        }
    }

    public function delete($id = '') {
        // check for delete permissions
        if (Session::get('options_access') < ACCESS_DELETE) {
            // redirect if not enough permission
            header('location: /login/');
            exit;
        } else {
            // delete option if enough permission
            $this->model->delete($id);
            header('location: /options/');
            exit;
        }
    }

}