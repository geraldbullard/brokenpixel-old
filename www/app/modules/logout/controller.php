<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: controller.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/logout/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class Logout extends Controller {

    function __construct() {
        parent::__construct();
        // check if logged in
        if (Session::get('loggedIn') != true) {
            header('Location: /login/');
            exit;
        }
    }
    
    function index() {
        // view the logout page
        $this->view->getTheme('site');
        $this->view->renderHeader();
        $this->view->renderView('logout/view/index');
        $this->view->renderFooter();
        $this->view->renderScripts();
        $this->view->renderBottom();
    }
    
    function run() {
        // run the logout process
        $this->model->run();
    }    

}