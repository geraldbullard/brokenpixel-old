<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: model.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/logout/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class logout_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    public function run() {
	    // insert the autdit trail
	    AuditTrail::log(
		    AuditTrail::USER_LOGOUT,
		    Session::get('usersData'),
		    'logout/',
		    'User Logout',
		    Session::get('usersId')
	    );
        Session::destroy();
        header('location: /login/');
        exit;
    }
    
}