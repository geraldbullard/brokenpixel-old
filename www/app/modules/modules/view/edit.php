<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: edit.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/options/view/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<div class="row">
    <div class="col-lg-12">
        <?php if (isset($_GET['msg']) && $_GET['msg'] != '') { ?>
        <div class="edit-msg">
            <?php
            if ($_GET['msg'] == 'edit_success') {
            $option = Option::getOptionById($_GET['setting']);
            ?>
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <i class="icon fa fa-check"></i> <?php echo $option['title']; ?> was updated successfully.
            </div>
            <?php } ?>
            <!--<div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <i class="icon fa fa-check"></i> Info alert preview. This alert is dismissable.
            </div>
            <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <i class="icon fa fa-check"></i> Warning alert preview. This alert is dismissable.
            </div>
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <i class="icon fa fa-check"></i> Danger alert preview. This alert is dismissable.
            </div>-->
        </div>
        <?php } ?>
        <div class="box">
            <div class="box-body no-padding">
                <?php if (count($this->settings) > 0) { ?>
                <table class="table">
                    <tr>
                        <th><?php echo $Language->get('text_title'); ?></th>
                        <th class="hide-below-1024"><?php echo $Language->get('text_define'); ?></th>
                        <th class="hide-below-480"><?php echo $Language->get('text_value'); ?></th>
                        <th class="text-right actions-column"><?php echo $Language->get('text_actions'); ?></th>
                    </tr>
                    <?php
                    foreach ($this->settings as $key => $data) {
                    ?>
                    <tr>
                        <td>
                            <p><?php echo $data['title']; ?> &nbsp; <i
                                class="fa fa-question-circle"
                                onclick="$('#summary_text_<?php echo $data['id']; ?>').toggle()"
                                style="cursor:pointer;"
                            ></i></p>
                            <div id="summary_text_<?php echo $data['id']; ?>" style="display:none;">
                                <p><small><?php echo $data['summary']; ?></small></p>
                            </div>
                        </td>
                        <td class="hide-below-1024"><?php echo $data['define']; ?></td>
                        <td class="hide-below-480"><?php echo $data['value']; ?></td>
                        <td>
                            <div class="pull-right btn-group">
                            <?php
                            $_disabled = false;
                            if (!Session::get('modules_access') >= ACCESS_EDIT) {
                                $_disabled = true;
                            }
                            echo '<button
                                class="btn btn-xs btn-primary"
                                type="button"
                                onclick="location.href=\'/modules/editsetting/' . 
                                    $data['id'] . '/?module=' . end(explode("/", $_GET['url'])) . '\'"
                                ' . ($_disabled ? ' disabled' : '') . '>
                                <i class="fa fa-pencil"></i> ' . $Language->get('text_edit') . '
                            </button>';
                            ?>
                            </div>
                        </td>
                    </tr>
                    <?php
                    }
                    ?>
                </table>
                <?php
                } else {
                    echo '<p style="padding:10px 0 0 10px;">' . $Language->get('text_no_module_settings') . '</p>';
                }
                ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-md-8 col-lg-6">
        <button
            type="button"
            class="cancel btn btn-lg btn-default"
            onclick="location.href='/modules/';">
            <?php
                echo $Language->get('text_back') . ' ' .
                $Language->get('text_to') . ' ' .
                $Language->get('text_modules');
            ?>
        </button>
    </div>
</div>
