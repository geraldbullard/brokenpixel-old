<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: index.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/modules/view/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body no-padding">
                <table class="table">
                    <tr>
                        <th width="1%"></th>
                        <th width="10%"><?php echo $Language->get('text_module'); ?></th>
                        <th width="39%"><?php echo $Language->get('text_summary'); ?></th>
                        <th width="5%" class="text-center"><?php echo $Language->get('text_settings'); ?></th>
                        <th width="5%" class="text-center"><?php echo $Language->get('text_system'); ?></th>
                        <th width="10%" class="text-center"><?php echo $Language->get('text_dependencies'); ?></th>
                        <th width="15%" class="text-right"><?php echo $Language->get('text_actions'); ?></th>
                    </tr>
                    <?php
                    foreach ($Modules as $module) {
                        echo '<tr>';
                        echo '    <input type="hidden" name="modules_id" value="' . $module['id'] . '">';
                        echo '    <td><i class="fa ' . $module['icon'] . '"></i></td>';
                        echo '    <td>' . $module['title'] . '</td>';
                        echo '    <td>' . $module['summary'] . '</td>';
                        echo '    <td class="text-center"><a href="/modules/edit/' . $module['id'] . '">' .
                             '        <button class="btn btn-xs btn-info">' .
                                          $Language->get('text_edit') .
                             '        </button>' .
                             '    </a></td>';
                        echo '    <td class="text-center"><button class="btn btn-xs' . (
                            $module['system'] == '1' ? ' btn-success' : ' btn-danger'
                            ) . '">' . (
                            $module['system'] == '1' ? $Language->get('text_yes') : $Language->get('text_no')
                        ) . '</button></td>';
                        echo '    <td class="text-center"><button class="btn btn-xs">- -</button></td>';
                        echo '    <td><div class="pull-right btn-group">';
                        if (Session::get('modules_access') >= ACCESS_EDIT && $module['status'] != '1') {
                            echo '<button
                                    class="btn btn-xs btn-primary"
                                    type="button"
                                    onclick="location.href=\'/modules/activate/' . $module['id'] . '\'">
                                    <i class="fa fa-pencil"></i> ' . $Language->get('text_activate') . '
                                  </button>';
                        } else {
                            echo '<button
                                 class="btn btn-xs' .
                                (($module['system'] == '1') ? ' btn-default' : ' btn-warning') . '"
                                 type="button"
                                 onclick="location.href=\'/modules/deactivate/' . $module['id'] . '\'"
                                 ' . (($module['system'] == '1') ? ' disabled="disabled"' : null) . '>
                                 <i class="fa fa-pencil"></i>
                                 ' . (
                                     ($module['system'] == '1') ?
                                     $Language->get('text_required') :
                                     $Language->get('text_deactivate')
                                 ) . '
                                 </button>';
                        }
                        $disable_delete = true;
                        if (Session::get('modules_access') >= ACCESS_DELETE) {
                            if ($module['system'] != '1' && $module['status'] != 1) {
	                            $disable_delete = false;
                            }
                            echo '<button
                                 class="delete-module btn btn-xs btn-danger"
                                 type="button"
                                 data-module-id="' . $module['id'] . '"
                                 ' . ($disable_delete ? ' disabled' : null) . '>
                                 <i class="fa fa-trash-o"></i> <span class="hide-below-480">' .
                                 $Language->get('text_delete') .
                                 '</span>
                                 </button>';
                        }
                        echo '    </div></td>';
                        echo '</tr>';
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>
