<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: index.js.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/modules/view/js/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<script>
    $(".delete-module").confirm({
        title: "<?php echo $Language->get('text_delete') . ' ' . $Language->get('text_module'); ?>",
        content: "<?php echo $Language->get('text_delete_module_confirm'); ?>",
        buttons: {
            ok: function() {
                location.href = '/modules/delete/' + this.$target.attr('data-module-id');
            },
            cancel: function() {
            }
        }
    });
</script>
