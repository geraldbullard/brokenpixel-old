<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: create-edit.js.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/modules/view/js/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
<script>
    $(".edit-cancel").confirm({
        title: "<?php echo $Language->get('text_go') . ' ' . $Language->get('text_back'); ?>?",
        content: "<?php echo $Language->get('text_back_to_module_settings_confirm'); ?>",
        buttons: {
            ok: function() {
                location.href = '/modules/edit/<?php echo @$_GET['module']; ?>';
            },
            cancel: function() {
            }
        }
    });
</script>
