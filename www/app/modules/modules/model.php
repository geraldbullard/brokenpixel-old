<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: model.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/modules/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

class Modules_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    public function modulesList() {
        return $this->db->select('SELECT ' . DB_PREFIX . 'modules.* FROM ' . DB_PREFIX . 'modules WHERE system != "1"');
    }

    public function modulesSingleList($type) {
        return $this->db->select(
            "SELECT *
            FROM " . DB_PREFIX . "options
            WHERE `type` = :type",
            array(':type' => $type)
        );
    }

    public function modulesSetting($id) {
        return $this->db->select(
            "SELECT *
            FROM " . DB_PREFIX . "options
            WHERE id = :id",
            array(':id' => $id)
        );
    }

    public function init() {
        global $Modules;
        foreach (new DirectoryIterator(MODULES) as $item) {
            if ($item->isDot()) continue;
            if ($item->isDir()) {
                $all[] = array(
                    'module' => $item->getFilename(),
                );
            }
        }
        foreach ($all as $mod) {
            if (!in_array($mod['module'], array_keys($Modules))) {
                $uninstalled[] = $mod['module'];
            }
        }
        if (@count((array)$uninstalled) > 0) {
            foreach ($uninstalled as $new) {
                if (is_file(MODULES . $new . '/init.php')) {
                    include(MODULES . $new . '/init.php');
                }
            }
        }
    }

    public function editsave($data) {
        $postData = array(
            'value' => $_POST['value']
        );
        $result = $this->db->update(DB_PREFIX . 'options', $postData, "`id` = {$data['id']}");
        // insert the autdit trail
        AuditTrail::log(
            AuditTrail::RECORD_EDITED,
            Session::get('usersData'),
            'modules/edit/',
            'Module Setting Edited', // AuditTrail::loadTypeById(AuditTrail::RECORD_EDITED)->getTitle()
            $data['id']
        );
        if ($result)
            return true;
        return false;
    }

    public function activate($id = '') {
        $this->db->update(DB_PREFIX . 'modules', array('status' => 1), "`id` = {$id}");
        // insert the autdit trail
        AuditTrail::log(
            AuditTrail::MODULE_ENABLED,
            Session::get('usersData'),
            'modules/activate/',
            'Module Activated',
            $id
        );
    }

    public function deactivate($id = '') {
        $this->db->update(DB_PREFIX . 'modules', array('status' => 0), "`id` = {$id}");
        // insert the autdit trail
        AuditTrail::log(
            AuditTrail::MODULE_DISABLED,
            Session::get('usersData'),
            'modules/deactivate/',
            'Module Deactivated',
            $id
        );
    }

    public function delete($id = '') {
        // remove the dir and all contents
        Utils::rmdir(MODULES . Module::getModuleCodeById($id));
        // remove from database
        $this->db->delete(DB_PREFIX . 'modules', "id = $id");
        $this->db->delete(DB_PREFIX . 'access', "module = '" . Module::getModuleCodeById($id) . "'");
        $this->db->delete(DB_PREFIX . 'options', "type = '" . Module::getModuleCodeById($id) . "'");
        $this->db->drop(DB_PREFIX . Module::getModuleCodeById($id));
        // insert the autdit trail
        AuditTrail::log(
            AuditTrail::MODULE_DELETED,
            Session::get('usersData'),
            'modules/delete/',
            'Module Deleted',
            $id
        );
    }

}
