<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access private
 * @author gnsPLANET, LLC.
 * @version $Id: menu.php, v1.0 2015-08-19 maestro Exp $
 * @location /app/modules/modules/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */

$_menu = array(
    'item' => '/modules',
    'title' => 'Modules',
    'icon' => 'fa-cubes',
    'system' => 1
);
