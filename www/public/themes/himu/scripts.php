<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: scripts.php, v1.0 2015-08-19 maestro Exp $
 * @location /public/themes/bootstrap/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
    <script src="/public/themes/himu/js/jquery.js"></script>
    <script src="/public/themes/himu/js/bootstrap.min.js"></script>
    <script src="/public/themes/himu/js/smoothscroll.js"></script>
    <script src="/public/themes/himu/js/jquery.isotope.min.js"></script>
    <script src="/public/themes/himu/js/jquery.prettyPhoto.js"></script>
    <script src="/public/themes/himu/js/jquery.parallax.js"></script>
    <script src="/public/themes/himu/js/main.js"></script>
