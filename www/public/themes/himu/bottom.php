<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: bottom.php, v1.0 2015-08-19 maestro Exp $
 * @location /public/themes/bootstrap/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
    global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;

    if (isset($_SESSION['usersRoleId']) && $_SESSION['usersRoleId'] < 3) {
?>
<div class="admin-bar">
    <div class="admin-bar-left">
        <!--<img src="/public/themes/adminlte/dist/img/favicon.png" title="Broken Pixel CMS" class="admin-bar-logo">-->
        <a href="/admin" title="Admin Dashboard">
            <i class="fa fa-dashboard"></i><span class="hide-below-768"> Dashboard</span></a>
        <?php if ($this->getContent()['id'] != '') { ?>
        <a href="/content/edit/<?php echo $this->getContent()['id']; ?>" title="Edit this Page">
            <i class="fa fa-pencil"></i><span class="hide-below-768"> Edit Page</span></a>
        <?php } ?>
    </div>
    <div class="admin-bar-right">
        <a href="/user/edit/<?php echo Session::get('usersId'); ?>" title="Profile">
            <i class="fa fa-fw fa-user"></i><span class="hide-below-768"> Profile</span></a>
        <a href="/logout/run" title="Logout">
            <i class="fa fa-fw fa-unlock"></i><span class="hide-below-768"> Logout</span></a>
    </div>
</div>
<?php
    }
?>
</body>
</html>