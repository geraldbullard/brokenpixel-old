<?php
/**
 ******************************** brokenPIXEL *******************************
 * @access public
 * @author gnsPLANET, LLC.
 * @version $Id: footer.php, v1.0 2015-08-19 maestro Exp $
 * @location /public/themes/bootstrap/
 *
 * @copyright Copyright (c) 2015, gnsPLANET, LLC.
 ******************************** brokenPIXEL *******************************
 */
global $Options, $Language, $Definitions, $Modules, $ContentTypes, $Menu;
?>
            </section>
            <!--<div class="close-tab">
                <i class="fa fa-angle-down"></i>
            </div>-->
        </div>
        <!-- Footer -->
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                Version 1.0.0
            </div>
            Copyright &copy; <?php
                echo date("Y");
            ?> <a href="https://www.brokenpixelcms.com/" target="_blank">brokenPIXEL CMS</a>. All rights reserved.
        </footer>
        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark" style="position: fixed; max-height: 100%; overflow: auto; padding-bottom: 50px;">
            <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                <li class="active">
                    <a href="#control-sidebar-activity" data-toggle="tab">
                        <i class="fa fa-history"></i>
                    </a>
                </li>
                <li>
                    <a href="#control-sidebar-notifications" data-toggle="tab">
                        <i class="fa fa-flag"></i>
                    </a>
                </li>
                <li>
                    <a href="#control-sidebar-settings" data-toggle="tab">
                        <i class="fa fa-gears"></i>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="control-sidebar-activity" class="tab-pane active">
                    <h4 class="control-sidebar-heading">
                        Recent Activity <span class="pull-right"><a href="/audit">See All</a></span>
                    </h4>
                    <ul class="control-sidebar-menu">
                        <?php
                        foreach (
                            AuditTrail::loadAll(
                                null,
                                'timestamp DESC',
                                $Options['core']['sidebarHistory']
                            ) as $key => $value
                        ) {
                            $audit_module = array_slice(explode("/", $value['url']), 0, 1)[0];
                            $audit_date = date_create(array_slice(explode(" ", $value['timestamp']), 0, 1)[0]);
                            $at_explode = explode(" ", $value['timestamp']);
                            $audit_time = date_create(end($at_explode));
                            /*
                            [url] => /user/edit/
                            [remote_host] => 127.0.0.1
                            [timestamp] => 2017-07-20 10:41:53
                            [user_agent] => Mozilla/5.0 (Windows NT 6.1; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0
                            [message] => User Updated.
                            [related_id] => 19*/
                        ?>
                        <li>
                            <a>
                                <i class="menu-icon fa <?php
                                    echo Module::getModuleIcon($audit_module);
                                ?> bg-blue" title="<?php
                                    echo Module::getModuleNameByModule($audit_module);
                                ?>"></i>
                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">
                                        <?php
                                            echo ucfirst($audit_module) . ' ' .
                                                AuditTrail::loadTypeById($value['audit_types_id'])->getActivityLabel() .
                                                ' (' . $value['related_id'] . ')';
                                        ?>
                                    </h4>
                                    <small class="white">
                                        <?php
                                            echo Users::getFullName($value['users_id']) . ' (' . $value['users_id'] . ')';
                                        ?><br />
                                        <?php
                                            echo date_format($audit_date, "M d, Y") . ' - ';
                                            echo date_format($audit_time, "h:i:s A");
                                        ?>
                                    </small>
                                </div>
                            </a>
                        </li>
                        <?php
                        }
                        ?>
                    </ul>
                    <!--<h3 class="control-sidebar-heading">Tasks Progress</h3>
                    <ul class="control-sidebar-menu">
                        <li>
                            <a href="javascript:void(0)">
                                <h4 class="control-sidebar-subheading">
                                    Custom Template Design
                                    <span class="label label-danger pull-right">70%</span>
                                </h4>
                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <h4 class="control-sidebar-subheading">
                                    Update Resume
                                    <span class="label label-success pull-right">95%</span>
                                </h4>
                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <h4 class="control-sidebar-subheading">
                                    Laravel Integration
                                    <span class="label label-warning pull-right">50%</span>
                                </h4>
                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <h4 class="control-sidebar-subheading">
                                    Back End Framework
                                    <span class="label label-primary pull-right">68%</span>
                                </h4>
                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                                </div>
                            </a>
                        </li>
                    </ul>-->
                </div>
                <div id="control-sidebar-notifications" class="tab-pane">
                    <div>
                        <h4 class="control-sidebar-heading">Notifications</h4>
                        <p>Panel Content</p>
                    </div>
                </div>
                <div id="control-sidebar-settings" class="tab-pane">
                    <form method="post">
                        <h4 class="control-sidebar-heading">Options & Settings</h4>
                        <div class="form-group">
                            <label class="control-sidebar-subheading">
                                Maintenance Mode
                                <input type="checkbox" class="pull-right">
                            </label>
                        </div>
                        <div class="form-group">
                            <label class="control-sidebar-subheading">
                                Delete chat history
                                <a href="javascript:void(0);" class="text-red pull-right">
                                    <i class="fa fa-trash-o"></i></a>
                            </label>
                        </div>
                    </form>
                </div>
            </div>
        </aside>
        <div class="control-sidebar-bg"></div>
    </div>