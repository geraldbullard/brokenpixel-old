# brokenPIXEL

This is a simple but very powerful CMS core built using MVC methodologies and practices. Class based, Object Oriented code in PHP. It has a very promising modular approach to a base framework that can be very powerful but simple to learn and develop. Oh, and it&#39;s 100% FREE!